Patch DDL de GARNUCHE du 23/10/2012 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1
-- Type : DDL
-- Schema : Garnuche
-- Numero de version : 1.116.11
-- Date de publication : 23/10/2012
-- Auteur(s) : Isabelle Réau
-- Licence : CeCILL version 2
--
WHENEVER SQLERROR EXIT SQL.SQLCODE;

------------------------------------------------------------
--  Creation de la table param_liste_diffusion qui definit les paramètres à afficher 
------------------------------------------------------------

CREATE TABLE GARNUCHE.PARAM_LISTE_DIFFUSION (	
	PARAM_ID NUMBER(38) NOT NULL, 
	LIBELLE_COURT VARCHAR2(100) NOT NULL, 
	LIBELLE_LONG VARCHAR2(255),
	VISIBILITE VARCHAR(3) NOT NULL,
	VALEUR_DEFAUT VARCHAR(1) NOT NULL, 
	D_CREATION DATE,
	D_MODIFICATION DATE,
	PERS_ID_CREATION NUMBER,
	PERS_ID_MODIFICATION NUMBER,
	CONSTRAINT PK_PARAM_ID PRIMARY KEY (PARAM_ID)
);

CREATE SEQUENCE GARNUCHE.PARAM_LISTE_DIFFUSION_SEQ START WITH 1;

COMMENT ON TABLE GARNUCHE.PARAM_LISTE_DIFFUSION  IS 'Liste des options à afficher';
COMMENT ON COLUMN GARNUCHE.PARAM_LISTE_DIFFUSION. PARAM_ID IS 'Clé primaire'; 
COMMENT ON COLUMN GARNUCHE.PARAM_LISTE_DIFFUSION.LIBELLE_COURT IS 'Libellé court affiché'; 
COMMENT ON COLUMN GARNUCHE.PARAM_LISTE_DIFFUSION.LIBELLE_LONG IS 'libellé long contenu dans l''info bulle'; 
COMMENT ON COLUMN GARNUCHE.PARAM_LISTE_DIFFUSION.VISIBILITE IS 'L,M,D ou une combinaison des trois pour indiquer quelle population voit la case'; 
COMMENT ON COLUMN GARNUCHE.PARAM_LISTE_DIFFUSION.VALEUR_DEFAUT IS 'O ou N pour indiquer si la case est cochée ou décochée par défaut'; 
COMMENT ON COLUMN GARNUCHE.PARAM_LISTE_DIFFUSION.D_CREATION IS 'Date de création'; 
COMMENT ON COLUMN GARNUCHE.PARAM_LISTE_DIFFUSION.D_MODIFICATION IS 'Date de modification';
COMMENT ON COLUMN GARNUCHE.PARAM_LISTE_DIFFUSION.PERS_ID_CREATION IS 'Pers_id de la personne à l''origine de la création de l''enregistrement';
COMMENT ON COLUMN GARNUCHE.PARAM_LISTE_DIFFUSION.PERS_ID_MODIFICATION IS 'Pers_id de la personne à l''origine de la modification de l''enregistrement';


------------------------------------------------------------
--  Creation de la table log_liste_diffusion (journal des choix des étudiants)
------------------------------------------------------------

CREATE TABLE GARNUCHE.LOG_LISTE_DIFFUSION (
	LOG_ID NUMBER(38) NOT NULL,	
	PARAM_ID NUMBER(38) NOT NULL, 
	NO_INDIVIDU NUMBER(8,0) NOT NULL, 
	VALEUR  VARCHAR(1) NOT NULL,
	D_CREATION DATE,
	CONSTRAINT PK_LOG_ID PRIMARY KEY (LOG_ID),
	CONSTRAINT FK_SCO_PARAM_ID FOREIGN KEY (PARAM_ID) REFERENCES GARNUCHE.PARAM_LISTE_DIFFUSION (PARAM_ID)
);

CREATE SEQUENCE GARNUCHE.LOG_LISTE_DIFFUSION_SEQ START WITH 1;

COMMENT ON TABLE GARNUCHE.LOG_LISTE_DIFFUSION  IS 'Liste des options à afficher';
COMMENT ON COLUMN GARNUCHE.LOG_LISTE_DIFFUSION. LOG_ID IS 'Clé primaire'; 
COMMENT ON COLUMN GARNUCHE.LOG_LISTE_DIFFUSION. PARAM_ID IS 'Id du paramètre à afficher (lien avec la table GARNUCHE.PARAM_LISTE_DIFFUSION )'; 
COMMENT ON COLUMN GARNUCHE.LOG_LISTE_DIFFUSION.NO_INDIVIDU IS 'Numéro de l''individu (étudiant)'; 
COMMENT ON COLUMN GARNUCHE.LOG_LISTE_DIFFUSION.VALEUR IS 'O ou N pour indiquer si la case est cochée ou décochée'; 
COMMENT ON COLUMN GARNUCHE.LOG_LISTE_DIFFUSION.D_CREATION IS 'Date de création'; 

