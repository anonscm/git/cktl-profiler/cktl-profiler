package org.cocktail.profiler.serveur.bdd;

import org.cocktail.fwkcktlwebapp.common.test.integ.NavigationHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.*;


import cucumber.api.java.fr.Alors;
import cucumber.api.java.fr.Lorsqu;
import cucumber.api.java.fr.Soit;

public class AuthentificationProfilerSteps {

	private WebDriver getWebDriver() {
		return NavigationHelper.getNavigationHelper().getWebDriver();
	}
    
    
    @Soit("^\"([^\"]*)\" habilité à se connecter avec le mot de passe \"([^\"]*)\"$")
    public void habilité_à_se_connecter_avec_le_mot_de_passe(String identification, String application) {
    	getWebDriver().navigate().to(NavigationHelper.URL_ACCUEIL);
    	getWebDriver().findElement(By.id("LoginId")).sendKeys(identification);
    }
//    Soit "grhumadm" connecté avec le mot de passe "achanger"
    @Lorsqu("^(?:il|elle) se connecte$")
    public void elle_se_connecte() {
    	WebElement webElement =  getWebDriver().findElement(By.name("mot_de_passe"));
    	getWebDriver().findElement(By.name("mot_de_passe")).sendKeys("achanger");
    	
    	WebElement boutonValiderLogin = getWebDriver().findElement(By.id("ValiderLoginId"));
    	boutonValiderLogin.click();
    }
    
    
    @Alors("^(?:il|elle) se retrouve sur la page d'accueil de Profiler$")
    public void elle_se_retrouve_sur_la_page_d_accueil_de_Profiler() throws Throwable {
    	Thread.sleep(5000);
        // Express the Regexp above with the code you wish you had    	
//    	System.out.println(getWebDriver().getPageSource());
    	assertTrue("PersId non trouvé dans la page", getWebDriver().getPageSource().contains("PersId"));
    	assertTrue("Coordonnées non trouvées dans la page", getWebDriver().getPageSource().contains("Coordonnées"));
    }
}
