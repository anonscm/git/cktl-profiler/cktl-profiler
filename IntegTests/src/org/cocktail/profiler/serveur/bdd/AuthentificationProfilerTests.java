package org.cocktail.profiler.serveur.bdd;

import org.junit.runner.RunWith;

import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@Cucumber.Options(features="classpath:authentificationProfiler.feature", glue = { "org.cocktail.fwkcktlwebapp.common.test.integ",
"org.cocktail.profiler.serveur.bdd" })
public class AuthentificationProfilerTests {
    
}
