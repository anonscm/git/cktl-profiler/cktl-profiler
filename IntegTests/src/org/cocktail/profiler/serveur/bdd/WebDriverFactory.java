package org.cocktail.profiler.serveur.bdd;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 */
public class WebDriverFactory {
	private static WebDriverFactory webDriverFactory;
	private WebDriver sharedWebDriver;
	
	public static WebDriverFactory getWebDriverFactory(){
		if (webDriverFactory==null){
			webDriverFactory= new WebDriverFactory();
		}
		return webDriverFactory;
	}
	
	public WebDriver getSharedWebDriver(){
		if (sharedWebDriver==null){
			sharedWebDriver= new ChromeDriver();
		}
		return sharedWebDriver;
	}
	
}
