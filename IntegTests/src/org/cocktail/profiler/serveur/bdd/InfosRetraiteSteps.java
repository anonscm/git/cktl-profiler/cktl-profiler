package org.cocktail.profiler.serveur.bdd;

import static org.junit.Assert.*;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.PendingException;
import cucumber.api.java.fr.Alors;
import cucumber.api.java.fr.Lorsqu;
import cucumber.api.java.fr.Soit;

public class InfosRetraiteSteps {
	@Alors("^l'affichage se fait en moins de \"([^\"]*)\" millisecondes$")
	public void l_affichage_se_fait_en_moins_de_millisecondes(String tpsMaxMilliseconds) throws Throwable {
		long tempsPasse=NavigationHelper.getNavigationHelper().getTpsClicLienEnMilliSec();
		long tempsMax=Long.parseLong(tpsMaxMilliseconds);
		assertTrue("Temps d'affichage ("+tempsPasse+" ms) supérieur au temps maximal attendu ("+tempsMax+" ms)",tempsPasse<tempsMax);
	}

	@Alors("^les colonnes du tableau détails sont \"([^\"]*)\"$")
	public void les_colonnes_du_tableau_détails_sont(String colonnes) throws Throwable {
		String[] tabColonnes=colonnes.split(",");
		int i=1;
		for (String col:tabColonnes) {
			CheckerHelper.assertTableHeaderCellEquals("listeElementsRetraiteTableViewId", i, col);
			i++;
		}
	}

	@Alors("^la ligne détail retraite numéro \"([^\"]*)\" contient \"([^\"]*)\"$")
	public void la_ligne_détail_retraite_numéro_contient(String numeroLigne, String colonnes) throws Throwable {
		String[] tabColonnes=colonnes.split(",");
		int i=1;
		for (String col:tabColonnes) {
			CheckerHelper.assertTableCellEquals("listeElementsRetraiteTableViewId", Integer.parseInt(numeroLigne), i, col);
			i++;
		}
	}

	@Alors("^le tableau détail retraite contient \"([^\"]*)\" colonnes et \"([^\"]*)\" lignes$")
	public void le_tableau_détail_retraite_contient_colonnes_et_lignes(String nbCols, String nbLignes) throws Throwable {
		CheckerHelper.assertTableSizeEquals("listeElementsRetraiteTableViewId", Integer.parseInt(nbLignes), Integer.parseInt(nbCols));
	}
}
