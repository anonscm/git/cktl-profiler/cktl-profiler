package org.cocktail.profiler.serveur.bdd;

import static org.junit.Assert.*;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.PendingException;
import cucumber.api.java.fr.Alors;
import cucumber.api.java.fr.Lorsqu;
import cucumber.api.java.fr.Soit;

public class EnfantsSteps {
	
	@Soit("^\"([^\"]*)\" connecté avec le mot de passe \"([^\"]*)\"$")
	public void connecté_avec_le_mot_de_passe(String login, String motDePasse) throws Throwable {
		NavigationHelper helper=NavigationHelper.getNavigationHelper(); 
		helper.connection(login, motDePasse);

		assertTrue(true);
	}
	
	@Lorsqu("^il va sur \"([^\"]*)\"$")
	public void il_va_sur(String texteLien) throws Throwable {
		NavigationHelper.getNavigationHelper().navigateToPageByLink(texteLien);
	}

	@Alors("^les colonnes du tableau enfants sont \"([^\"]*)\"$")
	public void les_colonnes_du_tableau_enfants_sont(String colonnes) throws Throwable {
		String[] tabColonnes=colonnes.split(",");
		int i=1;
		for (String col:tabColonnes) {
			CheckerHelper.assertTableHeaderCellEquals("listeEnfantsTableViewId", i, col);
			i++;
		}
	}

	@Alors("^la ligne enfants numéro \"([^\"]*)\" contient \"([^\"]*)\"$")
	public void la_ligne_enfants_numéro_contient(String numeroLigne, String colonnes) throws Throwable {
		String[] tabColonnes=colonnes.split(",");
		int i=1;
		for (String col:tabColonnes) {
			// Trim le résultat attendu car indique un espace quand le résultat doit être vide
			//   Problème de split si l'espace n'est pas rajouté dans la définition du test
			CheckerHelper.assertTableCellEquals("listeEnfantsTableViewId", Integer.parseInt(numeroLigne), i, col.trim());
			i++;
		}
	}
	
	@Alors("^aucun enfant n'est affiché$")
	public void aucun_enfant_n_est_affiché() throws Throwable {
		String xpath=".//*[@id='container']/table/tbody/tr[2]/td/div/div[2]/div";
		WebDriver webDriver=NavigationHelper.getNavigationHelper().getWebDriver();
		WebElement td = webDriver.findElement(By.xpath(xpath));
		assertEquals("Vous n'avez pas d'enfant renseigné.", td.getText());
	}
}
