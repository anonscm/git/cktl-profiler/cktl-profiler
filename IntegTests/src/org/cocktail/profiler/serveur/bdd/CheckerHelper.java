package org.cocktail.profiler.serveur.bdd;

import static org.junit.Assert.assertEquals;

import org.cocktail.fwkcktlwebapp.common.test.integ.NavigationHelper;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Classe permettant de faire des vérifications dans certaines parties de la page (par exemple sur des tableaux)
 * @author ffavreau
 *
 */
public class CheckerHelper {

	public static WebDriver getWebDriver() {
		return NavigationHelper.getNavigationHelper().getWebDriver();
	}
	
	/**
	 * Vérifie le contenu d'une cellule d'entête de tableau
	 * @param idTable Chaine permettant de retrouver le tableau désiré dans la page html. Le tableau retenu a un id contenant la chaine en paramètre. 
	 * @param numCol Numéro de la colonne testée (1=1ère colonne)
	 * @param contenuCelluleTh Valeur de cellule attendue
	 */
	public static void assertTableHeaderCellEquals(String idTable, int numCol, String contenuCelluleTh) {
		String xpath = ".//table[contains(@id,'" + idTable + "')]/thead/tr[1]/th[" + numCol + "]";
//		WebDriver webDriver = NavigationHelper.getNavigationHelper().getWebDriver();
		WebElement th=null;
		try {
			th = getWebDriver().findElement(By.xpath(xpath));
		} catch (Exception e) {
			Assert.fail("Exception raised when testing xpath: " + xpath);
		}
		assertEquals(contenuCelluleTh, th.getText());
	}

	/**
	 * Vérifie le contenu d'une cellule d'un tableau
	 * @param idTable Chaine permettant de retrouver le tableau désiré dans la page html. Le tableau retenu a un id contenant la chaine en paramètre. 
	 * @param numLigne Numéro de la ligne testée (1=1ère ligne)
	 * @param numCol Numéro de la colonne testée (1=1ère colonne)
	 * @param contenuCelluleTd Valeur de cellule attendue
	 */
	public static void assertTableCellEquals(String idTable, int numLigne, int numCol, String contenuCelluleTd) {
		String xpath = ".//table[contains(@id,'" + idTable + "')]/tbody/tr[" + numLigne + "]/td[" + numCol + "]";
//		WebDriver webDriver = NavigationHelper.getNavigationHelper().getWebDriver();
		WebElement td = null;
		try {
			td = getWebDriver().findElement(By.xpath(xpath));
		} catch (Exception e) {
			Assert.fail("Exception raised when testing xpath: " + xpath);
		}
		assertEquals(contenuCelluleTd, td.getText());
	}

	/**
	 * @param idTable Chaine permettant de retrouver le tableau désiré dans la page html. Le tableau retenu a un id contenant la chaine en paramètre. 
	 * @return Nombre de lignes du tableau dont l'id contient la chaine en paramètre. 0 si le tableau n'existe pas ou n'a pas de lignes.
	 */
	private static int getTableNbRows(String idTable) {
		WebElement tr = null;
//		WebDriver webDriver = NavigationHelper.getNavigationHelper().getWebDriver();
		String xpath;
		int i=1;
		boolean trouveLimite=false;
		// On limite à 9999 au cas où mais normalement, la fin de la boucle survient à cause du test sur le booleen
		while (i<9999 && !trouveLimite) {
			xpath = ".//table[contains(@id,'" + idTable + "')]/tbody/tr[" + i + "]";
			// Teste l'existance de la ligne i
			try {
				tr = getWebDriver().findElement(By.xpath(xpath));
				i++;
			} catch (Exception e) {
				trouveLimite=true;
			}		
		}
	
		// On diminue de 1 car si la ligne 5 n'existe pas par exemple, cela signifie que le tableau possède 4 lignes.
		return i-1; 
	}

	/**
	 * @param idTable Chaine permettant de retrouver le tableau désiré dans la page html. Le tableau retenu a un id contenant la chaine en paramètre. 
	 * @return Nombre de colonne du tableau dont l'id contient la chaine en paramètre. 0 si le tableau n'existe pas ou n'a pas de colonnes.
	 */
	private static int getTableNbCols(String idTable) {
		WebElement th = null;
//		WebDriver webDriver = NavigationHelper.getNavigationHelper().getWebDriver();
		String xpath;
		int i=1;
		boolean trouveLimite=false;
		// On limite à 9999 au cas où mais normalement, la fin de la boucle survient à cause du test sur le booleen
		while (i<9999 && !trouveLimite) {
			xpath = ".//table[contains(@id,'" + idTable + "')]/thead/tr[1]/th[" + i + "]";
			// Teste l'existance de la colonne i
			try {
				th = getWebDriver().findElement(By.xpath(xpath));
				i++;
			} catch (Exception e) {
				trouveLimite=true;
			}		
		}
	
		// On diminue de 1 car si la colonne 5 n'existe pas par exemple, cela signifie que le tableau possède 4 colonnes.
		return i-1; 
	}
	
	/**
	 * Test la taille d'un tableau d'id passé en paramètre
	 * @param idTable Chaine permettant de retrouver le tableau désiré dans la page html. Le tableau retenu a un id contenant la chaine en paramètre. 
	 * @param nLignes Nombre de de lignes attendues
	 * @param nCols Nombre de colonnes attendus
	 */
	public static void assertTableSizeEquals(String idTable, int nLignes, int nCols) {
		int rows=getTableNbRows(idTable);
		int cols=getTableNbCols(idTable);
		
		assertEquals(nLignes,rows);
		assertEquals(nCols,cols);
	}
	
	public static void allerSurOnglet(String ongletId) {
		WebDriverWait wait = new WebDriverWait(getWebDriver(), 10);
		String path = ongletId;

		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(path)));
		WebElement w=getWebDriver().findElement(By.xpath(path));
		w.click();
	}
}
