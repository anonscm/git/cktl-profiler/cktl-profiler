package org.cocktail.profiler.serveur.bdd;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.java.fr.Lorsqu;
import cucumber.api.java.fr.Soit;

public class NavigationSteps{
	
	public WebDriver getWebDriver() {
		return WebDriverFactory.getWebDriverFactory().getSharedWebDriver();
	}
	
	@Soit("^(\\w+) utilisateur connecté à  Girofle$")
	public void utilisateur_connecté_à_Girofle(String identification) throws Throwable {
		NavigationHelper.getNavigationHelper().connection(identification,"");
	}

	@Lorsqu("^elle va sur la page \"([^\"]*)\"$")
	public void elle_va_sur_la_page(String pageName) throws Throwable {
		NavigationHelper.getNavigationHelper().navigateToPage(pageName);
		WebDriverWait wait = new WebDriverWait(getWebDriver(), 5);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h2[contains(text(),\""+pageName+"\")]")));
				
		assertTrue(pageName + " pas trouvé ", getWebDriver().getPageSource().contains(pageName));
	}
}
