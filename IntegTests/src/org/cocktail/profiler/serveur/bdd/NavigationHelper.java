package org.cocktail.profiler.serveur.bdd;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * fournit des fonctions utiles pour naviguer dans Girofle
 */
public class NavigationHelper {
	// Private fields
	private static NavigationHelper navigationHelper;
	//private WebDriver webDriver;
	
	// Public fields
	public static final String URL_ACCUEIL = System.getProperty("URL_ACCUEIL");
	
	private long tpsClicLienEnMilliSec=0;
	
	public long getTpsClicLienEnMilliSec() {
		return tpsClicLienEnMilliSec;
	}


	// Properties
	public WebDriver getWebDriver() {
		return WebDriverFactory.getWebDriverFactory().getSharedWebDriver();
	}


	public static NavigationHelper getNavigationHelper() {
		if (navigationHelper == null) {
			navigationHelper = new NavigationHelper();
		}
		return navigationHelper;
	}

	// Public Methods
	public void connection(String identification, String motDePasse){
		getWebDriver().navigate().to(URL_ACCUEIL);	
		getWebDriver().findElement(By.id("LoginId")).sendKeys(identification);
		getWebDriver().findElement(By.name("mot_de_passe")).sendKeys(motDePasse);
		getWebDriver().findElement(By.name("validerLogin")).click();
	}
	
	public void navigateToPage(String pageName) {
		navigateToMenuTab(getTabNameForPage(pageName));
		navigateToPageByImageName(getImageNameForPage(pageName));
	}

	public void navigateToMenuTab(String tabName) {
		WebElement link = getWebDriver().findElement(By.id(tabName));
		link.click();
	}

	public void navigateToPageByImageName(String imageName) {
		String xpath = "//div[contains(@class,'placeholder')]/descendant::div[contains(@style, '$img')]/parent::a";
		xpath = xpath.replace("$img", imageName);
		By byXpathToSubmenuLink = By.xpath(xpath);
		WebElement linkSubmenu = getWebDriver().findElement(byXpathToSubmenuLink);
		linkSubmenu.click();
	}

	private String getTabNameForPage(String pageName) {
		if (pageName.equals("Liste des types d'Atomes Pédagogiques") ||
				pageName.equals("Liste des types d'Atomes d'Evaluations") ||
				pageName.equals("Liste des Diplômes")) {
			return "tabMaquettageId_tabId";
		}
		return "";
	}

	private String getImageNameForPage(String pageName) {
		if (pageName.equals("Liste des types d'Atomes Pédagogiques")) {
			return "APE-1.png";
		}
		if (pageName.equals("Liste des Diplômes")) {
			return "DIP-1.png";
		}
		if (pageName.equals("Liste des types d'Atomes d'Evaluations")) {
			return "EVA-1.png";
		}
		return "";
	}
	
	/**
	 * navigue vers une page en cliquant sur un lien
	 * @param linkText texte du lien
	 */
	public void navigateToPageByLink(String linkText) {
		String xpath = "//a[contains(text(),'" + linkText + "')]";
		WebDriver webDriver=getWebDriver();
		WebDriverWait wait = new WebDriverWait(webDriver, 5);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));

		WebElement link = webDriver.findElement(By.xpath(xpath));
		long lStartTime = System.currentTimeMillis();
		link.click();
		long lEndTime = System.currentTimeMillis();
		tpsClicLienEnMilliSec = lEndTime - lStartTime;
	}
}
