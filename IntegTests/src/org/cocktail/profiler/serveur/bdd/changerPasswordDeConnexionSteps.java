package org.cocktail.profiler.serveur.bdd;

import static org.cocktail.profiler.serveur.bdd.CheckerHelper.*;
import static org.junit.Assert.assertTrue;

import org.cocktail.fwkcktlwebapp.common.test.integ.NavigationHelper;
import org.openqa.selenium.WebDriver;

import cucumber.api.PendingException;
import cucumber.api.java.fr.Alors;
import cucumber.api.java.fr.Lorsqu;

public class changerPasswordDeConnexionSteps {

	private WebDriver getWebDriver() {
		return NavigationHelper.getNavigationHelper().getWebDriver();
	}
	
	
	@Lorsqu("^il va sur l'onglet \"([^\"]*)\"$")
	public void il_va_sur_l_onglet(String nomOnglet) throws Throwable {
	    
		String ongletId = "//*[@id='goCompteId']";
		allerSurOnglet(ongletId);
	  //*[@id="goCompteId"]
	}

	@Alors("^il se retrouve sur la page contenant \"([^\"]*)\"$")
	public void il_se_retrouve_sur_la_page_contenant(String libelleChamps) throws Throwable {
	    
		assertTrue(libelleChamps + " non trouvé dans la page", getWebDriver().getPageSource().contains(libelleChamps));
	}
}
