# language: fr
Fonctionnalité: Authentification hors CAS
  Afin d'accéder à l'application Profiler
  En tant qu'utilisateur du PGI Cocktail
  Je dois m'identifier à l'aide de mon identifiant et de mon mot de passe

  Scénario: 1 - Authentification d'un utilisateur habilité qui est le GRHUM Créateur
    Soit "grhumadm" habilité à se connecter avec le mot de passe "achanger"
 #   Soit "grhumadm" utilisateur connecté à "Profiler" avec le mot de passe "achanger"
 #   Soit "alexia" utilisateur connecté à "Profiler" avec le mot de passe "motDePasse"
    	Lorsqu'il se connecte
    	Alors il se retrouve sur la page d'accueil de Profiler

    	
  Scénario: 2 - Authentification d'un utilisateur habilité qui est Alexia DUPONT
    Soit "alexia" utilisateur connecté à "Profiler" avec le mot de passe "motDePasse"
    #	Lorsqu'il se connecte
    	Alors il se retrouve sur la page d'accueil de Profiler