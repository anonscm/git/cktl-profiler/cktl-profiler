# language: fr
Fonctionnalité: Changer le mot de passe de connexion
  Afin de changer le mot de passe
  En tant qu'utilisateur de Profiler
  Je dois aller sur l'onglet Comptes puis cliquer sur changer le mot de passe puis renseigner les champs du nouveau mot de passe puis cliquer sur valider

##  Scénario: Changement du mot de passe de connexion
##    Soit alain connecté à Profiler
##    Lorsqu'il va sur l'onglet "Comptes"
##    Lorsqu'il renseigne le nouveau mot de passe avec "9zz"
##    Alors le mot de passe a bien été changé
    
  Scénario: 1 - Authentification d'un utilisateur habilité qui est le GRHUM Créateur
    Soit "grhumadm" habilité à se connecter avec le mot de passe "achanger"
 #   Soit "grhumadm" utilisateur connecté à "Profiler" avec le mot de passe "achanger"
 #   Soit "alexia" utilisateur connecté à "Profiler" avec le mot de passe "motDePasse"
    	Lorsqu'il se connecte
    	Alors il se retrouve sur la page d'accueil de Profiler

  Scénario: 2 - Accès à l'onglet des comptes
    	Lorsqu'il va sur l'onglet "Comptes"
    	Alors il se retrouve sur la page contenant "Comptes informatiques" 