# language: fr
Fonctionnalité: Affichage des données relatives aux infos retraite
	Créé/testé sur cockdev

#  Scénario: 18 activités, 5 services validés, 1 CDD, 1 service militaire
#    Soit "kmazzon" connecté avec le mot de passe "kmazzon1"
#    Lorsqu'il va sur "Infos retraite"
#    Alors l'affichage se fait en moins de "3000" millisecondes
#    Alors les colonnes du tableau détails sont "Position,Localisation,Début,Fin,Quotité,Années,Mois,Jours"
#    Alors la ligne détail retraite numéro "1" contient "Activité,UFR SCIENCES ET TECHNOLOGIE UNIVERSITE LA ROCHELLE,21/08/2014,08/09/2014,100,0,0,18"
#    Alors la ligne détail retraite numéro "2" contient "Activité,UFR SCIENCES ET TECHNOLOGIE UNIVERSITE LA ROCHELLE,01/09/2013,20/08/2014,100,0,11,20"
#    Alors la ligne détail retraite numéro "3" contient "Activité,UFR SCIENCES ET TECHNOLOGIE UNIVERSITE LA ROCHELLE,01/09/2009,30/09/2010,100,1,1,0"
#    Alors la ligne détail retraite numéro "4" contient "Activité,UFR SCIENCES ET TECHNOLOGIE UNIVERSITE LA ROCHELLE,01/01/2009,31/08/2009,100,0,8,0"
#    Alors la ligne détail retraite numéro "5" contient "Activité,UFR SCIENCES ET TECHNOLOGIE UNIVERSITE LA ROCHELLE,01/02/2008,31/12/2008,100,0,11,0"
#    Alors la ligne détail retraite numéro "6" contient "Activité,UFR SCIENCES ET TECHNOLOGIE UNIVERSITE LA ROCHELLE,01/01/2007,31/01/2008,100,1,1,0"
#    Alors la ligne détail retraite numéro "7" contient "Activité,UFR SCIENCES ET TECHNOLOGIE UNIVERSITE LA ROCHELLE,01/11/2006,31/12/2006,100,0,2,0"
#    Alors la ligne détail retraite numéro "8" contient "Activité,UFR SCIENCES ET TECHNOLOGIE UNIVERSITE LA ROCHELLE,01/09/2006,31/10/2006,80,0,1,18"
#    Alors la ligne détail retraite numéro "9" contient "Activité,UFR SCIENCES ET TECHNOLOGIE UNIVERSITE LA ROCHELLE,01/04/2006,31/08/2006,100,0,5,0"
#    Alors la ligne détail retraite numéro "10" contient "Activité,UFR SCIENCES ET TECHNOLOGIE UNIVERSITE LA ROCHELLE,01/10/2005,31/03/2006,100,0,6,0"
#    Alors la ligne détail retraite numéro "11" contient "Activité,UFR SCIENCES ET TECHNOLOGIE UNIVERSITE LA ROCHELLE,01/09/2005,30/09/2005,100,0,1,0"
#    Alors la ligne détail retraite numéro "12" contient "Activité,UFR SCIENCES ET TECHNOLOGIE UNIVERSITE LA ROCHELLE,01/09/2004,31/08/2005,80,0,9,18"
#    Alors la ligne détail retraite numéro "13" contient "Activité,UFR SCIENCES ET TECHNOLOGIE UNIVERSITE LA ROCHELLE,01/06/2004,31/08/2004,80,0,2,12"
#    Alors la ligne détail retraite numéro "14" contient "Activité,UFR SCIENCES ET TECHNOLOGIE UNIVERSITE LA ROCHELLE,01/03/2004,31/05/2004,80,0,2,12"
#    Alors la ligne détail retraite numéro "15" contient "Activité,UFR SCIENCES ET TECHNOLOGIE UNIVERSITE LA ROCHELLE,01/09/2002,29/02/2004,100,1,6,0"
#    Alors la ligne détail retraite numéro "16" contient "Activité,UNIVERSITE LA ROCHELLE,01/08/2002,31/08/2002,100,0,1,0"
#    Alors la ligne détail retraite numéro "17" contient "Activité,UNIVERSITE LA ROCHELLE,01/09/2000,31/07/2002,100,1,11,0"
#    Alors la ligne détail retraite numéro "18" contient "Activité,UNIVERSITE LA ROCHELLE,01/09/1999,31/08/2000,100,1,0,0"
#    Alors la ligne détail retraite numéro "19" contient "CDD,LYCEE PROFESSIONNEL MAGENTA,01/09/1998,31/08/1999,100.0,1,0,0"
#    Alors la ligne détail retraite numéro "20" contient "SVAL,RECTORAT DE BORDEAUX,01/09/1993,31/08/1998,100,5,0,0"
#    Alors la ligne détail retraite numéro "21" contient "SVAL,RECTORAT DE BORDEAUX,07/09/1992,31/08/1993,100,0,10,19"
#    Alors la ligne détail retraite numéro "22" contient "SVAL,RECTORAT DE BORDEAUX,11/12/1990,31/08/1992,100,1,8,20"
#    Alors la ligne détail retraite numéro "23" contient "SVAL,RECTORAT DE BORDEAUX,12/09/1990,06/12/1990,100,0,2,25"
#    Alors la ligne détail retraite numéro "24" contient "SVAL,RECTORAT DE BORDEAUX,11/06/1990,27/07/1990,100,0,1,17"
#    Alors la ligne détail retraite numéro "25" contient "Svc Militaire,RDV Citoyen,01/01/1988,31/10/1988,100,0,10,0"
#    Alors le tableau détail retraite contient "8" colonnes et "25" lignes
    