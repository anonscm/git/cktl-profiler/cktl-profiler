# language: fr
Fonctionnalité: Affichage des données relatives aux enfants
	Créé/testé sur cockdev

#  Scénario: 6 enfants légitimes dont 1 décédé
#    Soit "jbelmond" connecté avec le mot de passe "AXYpa9E:"
#    Lorsqu'il va sur "Infos personnelles"
#    Alors les colonnes du tableau enfants sont "Prénom,Nom,Sexe,Naissance,Type filiation,Date de décès"
#    Alors la ligne enfants numéro "1" contient "NATHAN,BELMONDO,M,25/05/2010,LEGITIME, "
#    Alors la ligne enfants numéro "2" contient "JULIAN,BELMONDO,M,14/04/2008,LEGITIME, "
#    Alors la ligne enfants numéro "3" contient "ANNA,BELMONDO,F,13/06/2005,LEGITIME,10/01/2009"
#    Alors la ligne enfants numéro "4" contient "ALEX,BELMONDO,F,31/10/2002,LEGITIME, "
#    Alors la ligne enfants numéro "5" contient "CHELSEA,BELMONDO,F,21/10/1998,LEGITIME, "
#    Alors la ligne enfants numéro "6" contient "ANITA,BELMONDO,F,15/09/1996,LEGITIME, "
 
#  Scénario: 1 enfant adopté
#    Soit "cdeneuve" connecté avec le mot de passe "htb43JQsmJpcc"
#    Lorsqu'il va sur "Infos personnelles"
#    Alors les colonnes du tableau enfants sont "Prénom,Nom,Sexe,Naissance,Type filiation,Date d'arrivée au foyer"
#    Alors la ligne enfants numéro "1" contient "CARLA,DENEUVE,F,10/12/2013,ADOPTE,01/04/2014"
    
#  Scénario: Aucun enfant
#    Soit "edepart" connecté avec le mot de passe "R52IlTNGK4dAs"
#    Lorsqu'il va sur "Infos personnelles"
#    Alors aucun enfant n'est affiché
    