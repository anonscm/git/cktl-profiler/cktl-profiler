package org.cocktail.profiler.server.eof;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktldroitsutils.common.util.MyDateCtrl;

public class EOLogListeDiffusion extends _EOLogListeDiffusion {
  private static Logger log = Logger.getLogger(EOLogListeDiffusion.class);
  
  public EOLogListeDiffusion() {
	  super();
  }
  
  public EOLogListeDiffusion(Long paramId, Integer noIndividu, String valeur) {
	  super();
	  this.setParamId(paramId);
	  this.setNoIndividu(noIndividu);
	  this.setValeur(valeur);
	 /* if (valeur){
		  this.setValeur("O");
	  } else {
		  this.setValeur("N");
	  }*/
	  this.setDateCreation(MyDateCtrl.now());
  }
  
}
