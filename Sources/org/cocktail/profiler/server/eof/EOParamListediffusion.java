package org.cocktail.profiler.server.eof;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;

public class EOParamListediffusion extends _EOParamListediffusion {
  private static Logger log = Logger.getLogger(EOParamListediffusion.class);
  private boolean caseCochee ;
  
	  public EOParamListediffusion() {
		  super();
		  if (super.valeurDefaut() != null){
			  this.setCaseCochee( MyStringCtrl.isChaineOui(super.valeurDefaut()));
		  } else {
			  this.setCaseCochee(false);
		  }
	  }
	  public EOParamListediffusion(boolean caseCochee ) {
		  super();
		  this.setCaseCochee(caseCochee);
		  
	  }
	/**
	 * @return the valeur
	 */
	public boolean isCaseCochee() {
		return caseCochee;
	}
	/**
	 * @param valeur the valeur to set
	 */
	public void setCaseCochee(boolean caseCochee) {
		this.caseCochee = caseCochee;
	}
	
  
}
