// $LastChangedRevision: 5810 $ DO NOT EDIT.  Make changes to EOLogListeDiffusion.java instead.
package org.cocktail.profiler.server.eof;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOLogListeDiffusion extends  EOGenericRecord {
	public static final String ENTITY_NAME = "LogListeDiffusion";

	// Attributes
	public static final String DATE_CREATION_KEY = "dateCreation";
	public static final String NO_INDIVIDU_KEY = "noIndividu";
	public static final String PARAM_ID_KEY = "paramId";
	public static final String VALEUR_KEY = "valeur";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOLogListeDiffusion.class);

  public EOLogListeDiffusion localInstanceIn(EOEditingContext editingContext) {
    EOLogListeDiffusion localInstance = (EOLogListeDiffusion)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dateCreation() {
    return (NSTimestamp) storedValueForKey("dateCreation");
  }

  public void setDateCreation(NSTimestamp value) {
    if (_EOLogListeDiffusion.LOG.isDebugEnabled()) {
    	_EOLogListeDiffusion.LOG.debug( "updating dateCreation from " + dateCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dateCreation");
  }

  public Integer noIndividu() {
    return (Integer) storedValueForKey("noIndividu");
  }

  public void setNoIndividu(Integer value) {
    if (_EOLogListeDiffusion.LOG.isDebugEnabled()) {
    	_EOLogListeDiffusion.LOG.debug( "updating noIndividu from " + noIndividu() + " to " + value);
    }
    takeStoredValueForKey(value, "noIndividu");
  }

  public Long paramId() {
    return (Long) storedValueForKey("paramId");
  }

  public void setParamId(Long value) {
    if (_EOLogListeDiffusion.LOG.isDebugEnabled()) {
    	_EOLogListeDiffusion.LOG.debug( "updating paramId from " + paramId() + " to " + value);
    }
    takeStoredValueForKey(value, "paramId");
  }

  public String valeur() {
    return (String) storedValueForKey("valeur");
  }

  public void setValeur(String value) {
    if (_EOLogListeDiffusion.LOG.isDebugEnabled()) {
    	_EOLogListeDiffusion.LOG.debug( "updating valeur from " + valeur() + " to " + value);
    }
    takeStoredValueForKey(value, "valeur");
  }


  public static EOLogListeDiffusion createLogListeDiffusion(EOEditingContext editingContext, Integer noIndividu
, Long paramId
, String valeur
) {
    EOLogListeDiffusion eo = (EOLogListeDiffusion) EOUtilities.createAndInsertInstance(editingContext, _EOLogListeDiffusion.ENTITY_NAME);    
		eo.setNoIndividu(noIndividu);
		eo.setParamId(paramId);
		eo.setValeur(valeur);
    return eo;
  }

  public static NSArray<EOLogListeDiffusion> fetchAllLogListeDiffusions(EOEditingContext editingContext) {
    return _EOLogListeDiffusion.fetchAllLogListeDiffusions(editingContext, null);
  }

  public static NSArray<EOLogListeDiffusion> fetchAllLogListeDiffusions(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOLogListeDiffusion.fetchLogListeDiffusions(editingContext, null, sortOrderings);
  }

  public static NSArray<EOLogListeDiffusion> fetchLogListeDiffusions(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOLogListeDiffusion.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOLogListeDiffusion> eoObjects = (NSArray<EOLogListeDiffusion>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOLogListeDiffusion fetchLogListeDiffusion(EOEditingContext editingContext, String keyName, Object value) {
    return _EOLogListeDiffusion.fetchLogListeDiffusion(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOLogListeDiffusion fetchLogListeDiffusion(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOLogListeDiffusion> eoObjects = _EOLogListeDiffusion.fetchLogListeDiffusions(editingContext, qualifier, null);
    EOLogListeDiffusion eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOLogListeDiffusion)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one LogListeDiffusion that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOLogListeDiffusion fetchRequiredLogListeDiffusion(EOEditingContext editingContext, String keyName, Object value) {
    return _EOLogListeDiffusion.fetchRequiredLogListeDiffusion(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOLogListeDiffusion fetchRequiredLogListeDiffusion(EOEditingContext editingContext, EOQualifier qualifier) {
    EOLogListeDiffusion eoObject = _EOLogListeDiffusion.fetchLogListeDiffusion(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no LogListeDiffusion that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOLogListeDiffusion localInstanceIn(EOEditingContext editingContext, EOLogListeDiffusion eo) {
    EOLogListeDiffusion localInstance = (eo == null) ? null : (EOLogListeDiffusion)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
