// $LastChangedRevision: 5810 $ DO NOT EDIT.  Make changes to EOScolInscriptionElt.java instead.
package org.cocktail.profiler.server.eof;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOScolInscriptionElt extends  EOGenericRecord {
	public static final String ENTITY_NAME = "ScolInscriptionElt";

	// Attributes
	public static final String EELT_CODE_KEY = "eeltCode";
	public static final String IEREL_DISPENSE_KEY = "ierelDispense";
	public static final String IEREL_ETAT_KEY = "ierelEtat";
	public static final String IEREL_MOYENNE_KEY = "ierelMoyenne";
	public static final String IEREL_SEMESTRE_KEY = "ierelSemestre";

	// Relationships
	public static final String TO_SCOL_FORMATION_ANNEE_KEY = "toScolFormationAnnee";
	public static final String TO_SCOL_INSCRIPTION_ETUDIANT_KEY = "toScolInscriptionEtudiant";
	public static final String TO_SCOL_MAQUETTE_EC_KEY = "toScolMaquetteEc";

  private static Logger LOG = Logger.getLogger(_EOScolInscriptionElt.class);

  public EOScolInscriptionElt localInstanceIn(EOEditingContext editingContext) {
    EOScolInscriptionElt localInstance = (EOScolInscriptionElt)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String eeltCode() {
    return (String) storedValueForKey("eeltCode");
  }

  public void setEeltCode(String value) {
    if (_EOScolInscriptionElt.LOG.isDebugEnabled()) {
    	_EOScolInscriptionElt.LOG.debug( "updating eeltCode from " + eeltCode() + " to " + value);
    }
    takeStoredValueForKey(value, "eeltCode");
  }

  public Integer ierelDispense() {
    return (Integer) storedValueForKey("ierelDispense");
  }

  public void setIerelDispense(Integer value) {
    if (_EOScolInscriptionElt.LOG.isDebugEnabled()) {
    	_EOScolInscriptionElt.LOG.debug( "updating ierelDispense from " + ierelDispense() + " to " + value);
    }
    takeStoredValueForKey(value, "ierelDispense");
  }

  public Integer ierelEtat() {
    return (Integer) storedValueForKey("ierelEtat");
  }

  public void setIerelEtat(Integer value) {
    if (_EOScolInscriptionElt.LOG.isDebugEnabled()) {
    	_EOScolInscriptionElt.LOG.debug( "updating ierelEtat from " + ierelEtat() + " to " + value);
    }
    takeStoredValueForKey(value, "ierelEtat");
  }

  public Float ierelMoyenne() {
    return (Float) storedValueForKey("ierelMoyenne");
  }

  public void setIerelMoyenne(Float value) {
    if (_EOScolInscriptionElt.LOG.isDebugEnabled()) {
    	_EOScolInscriptionElt.LOG.debug( "updating ierelMoyenne from " + ierelMoyenne() + " to " + value);
    }
    takeStoredValueForKey(value, "ierelMoyenne");
  }

  public Integer ierelSemestre() {
    return (Integer) storedValueForKey("ierelSemestre");
  }

  public void setIerelSemestre(Integer value) {
    if (_EOScolInscriptionElt.LOG.isDebugEnabled()) {
    	_EOScolInscriptionElt.LOG.debug( "updating ierelSemestre from " + ierelSemestre() + " to " + value);
    }
    takeStoredValueForKey(value, "ierelSemestre");
  }

  public org.cocktail.profiler.server.eof.EOScolFormationAnnee toScolFormationAnnee() {
    return (org.cocktail.profiler.server.eof.EOScolFormationAnnee)storedValueForKey("toScolFormationAnnee");
  }

  public void setToScolFormationAnneeRelationship(org.cocktail.profiler.server.eof.EOScolFormationAnnee value) {
    if (_EOScolInscriptionElt.LOG.isDebugEnabled()) {
      _EOScolInscriptionElt.LOG.debug("updating toScolFormationAnnee from " + toScolFormationAnnee() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.profiler.server.eof.EOScolFormationAnnee oldValue = toScolFormationAnnee();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toScolFormationAnnee");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toScolFormationAnnee");
    }
  }
  
  public org.cocktail.profiler.server.eof.EOScolInscriptionEtudiant toScolInscriptionEtudiant() {
    return (org.cocktail.profiler.server.eof.EOScolInscriptionEtudiant)storedValueForKey("toScolInscriptionEtudiant");
  }

  public void setToScolInscriptionEtudiantRelationship(org.cocktail.profiler.server.eof.EOScolInscriptionEtudiant value) {
    if (_EOScolInscriptionElt.LOG.isDebugEnabled()) {
      _EOScolInscriptionElt.LOG.debug("updating toScolInscriptionEtudiant from " + toScolInscriptionEtudiant() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.profiler.server.eof.EOScolInscriptionEtudiant oldValue = toScolInscriptionEtudiant();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toScolInscriptionEtudiant");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toScolInscriptionEtudiant");
    }
  }
  
  public org.cocktail.profiler.server.eof.EOScolMaquetteEc toScolMaquetteEc() {
    return (org.cocktail.profiler.server.eof.EOScolMaquetteEc)storedValueForKey("toScolMaquetteEc");
  }

  public void setToScolMaquetteEcRelationship(org.cocktail.profiler.server.eof.EOScolMaquetteEc value) {
    if (_EOScolInscriptionElt.LOG.isDebugEnabled()) {
      _EOScolInscriptionElt.LOG.debug("updating toScolMaquetteEc from " + toScolMaquetteEc() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.profiler.server.eof.EOScolMaquetteEc oldValue = toScolMaquetteEc();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toScolMaquetteEc");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toScolMaquetteEc");
    }
  }
  

  public static EOScolInscriptionElt createScolInscriptionElt(EOEditingContext editingContext, String eeltCode
, Integer ierelDispense
, Integer ierelEtat
, Float ierelMoyenne
, Integer ierelSemestre
, org.cocktail.profiler.server.eof.EOScolFormationAnnee toScolFormationAnnee, org.cocktail.profiler.server.eof.EOScolInscriptionEtudiant toScolInscriptionEtudiant, org.cocktail.profiler.server.eof.EOScolMaquetteEc toScolMaquetteEc) {
    EOScolInscriptionElt eo = (EOScolInscriptionElt) EOUtilities.createAndInsertInstance(editingContext, _EOScolInscriptionElt.ENTITY_NAME);    
		eo.setEeltCode(eeltCode);
		eo.setIerelDispense(ierelDispense);
		eo.setIerelEtat(ierelEtat);
		eo.setIerelMoyenne(ierelMoyenne);
		eo.setIerelSemestre(ierelSemestre);
    eo.setToScolFormationAnneeRelationship(toScolFormationAnnee);
    eo.setToScolInscriptionEtudiantRelationship(toScolInscriptionEtudiant);
    eo.setToScolMaquetteEcRelationship(toScolMaquetteEc);
    return eo;
  }

  public static NSArray<EOScolInscriptionElt> fetchAllScolInscriptionElts(EOEditingContext editingContext) {
    return _EOScolInscriptionElt.fetchAllScolInscriptionElts(editingContext, null);
  }

  public static NSArray<EOScolInscriptionElt> fetchAllScolInscriptionElts(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOScolInscriptionElt.fetchScolInscriptionElts(editingContext, null, sortOrderings);
  }

  public static NSArray<EOScolInscriptionElt> fetchScolInscriptionElts(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOScolInscriptionElt.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOScolInscriptionElt> eoObjects = (NSArray<EOScolInscriptionElt>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOScolInscriptionElt fetchScolInscriptionElt(EOEditingContext editingContext, String keyName, Object value) {
    return _EOScolInscriptionElt.fetchScolInscriptionElt(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOScolInscriptionElt fetchScolInscriptionElt(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOScolInscriptionElt> eoObjects = _EOScolInscriptionElt.fetchScolInscriptionElts(editingContext, qualifier, null);
    EOScolInscriptionElt eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOScolInscriptionElt)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one ScolInscriptionElt that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOScolInscriptionElt fetchRequiredScolInscriptionElt(EOEditingContext editingContext, String keyName, Object value) {
    return _EOScolInscriptionElt.fetchRequiredScolInscriptionElt(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOScolInscriptionElt fetchRequiredScolInscriptionElt(EOEditingContext editingContext, EOQualifier qualifier) {
    EOScolInscriptionElt eoObject = _EOScolInscriptionElt.fetchScolInscriptionElt(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no ScolInscriptionElt that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOScolInscriptionElt localInstanceIn(EOEditingContext editingContext, EOScolInscriptionElt eo) {
    EOScolInscriptionElt localInstance = (eo == null) ? null : (EOScolInscriptionElt)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
