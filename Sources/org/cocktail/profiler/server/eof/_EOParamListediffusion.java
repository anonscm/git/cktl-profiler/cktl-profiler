// $LastChangedRevision: 5810 $ DO NOT EDIT.  Make changes to EOParamListediffusion.java instead.
package org.cocktail.profiler.server.eof;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOParamListediffusion extends  EOGenericRecord {
	public static final String ENTITY_NAME = "ParamListediffusion";

	// Attributes
	public static final String DATE_CREATION_KEY = "dateCreation";
	public static final String DATE_MODIFICATION_KEY = "dateModification";
	public static final String LIBELLE_COURT_KEY = "libelleCourt";
	public static final String LIBELLE_LONG_KEY = "libelleLong";
	public static final String PARAM_ID_KEY = "paramId";
	public static final String PERS_ID_CREATION_KEY = "persIdCreation";
	public static final String PERS_ID_MODIFICATION_KEY = "persIdModification";
	public static final String VALEUR_DEFAUT_KEY = "valeurDefaut";
	public static final String VISIBILITE_KEY = "visibilite";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOParamListediffusion.class);

  public EOParamListediffusion localInstanceIn(EOEditingContext editingContext) {
    EOParamListediffusion localInstance = (EOParamListediffusion)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dateCreation() {
    return (NSTimestamp) storedValueForKey("dateCreation");
  }

  public void setDateCreation(NSTimestamp value) {
    if (_EOParamListediffusion.LOG.isDebugEnabled()) {
    	_EOParamListediffusion.LOG.debug( "updating dateCreation from " + dateCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dateCreation");
  }

  public NSTimestamp dateModification() {
    return (NSTimestamp) storedValueForKey("dateModification");
  }

  public void setDateModification(NSTimestamp value) {
    if (_EOParamListediffusion.LOG.isDebugEnabled()) {
    	_EOParamListediffusion.LOG.debug( "updating dateModification from " + dateModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dateModification");
  }

  public String libelleCourt() {
    return (String) storedValueForKey("libelleCourt");
  }

  public void setLibelleCourt(String value) {
    if (_EOParamListediffusion.LOG.isDebugEnabled()) {
    	_EOParamListediffusion.LOG.debug( "updating libelleCourt from " + libelleCourt() + " to " + value);
    }
    takeStoredValueForKey(value, "libelleCourt");
  }

  public String libelleLong() {
    return (String) storedValueForKey("libelleLong");
  }

  public void setLibelleLong(String value) {
    if (_EOParamListediffusion.LOG.isDebugEnabled()) {
    	_EOParamListediffusion.LOG.debug( "updating libelleLong from " + libelleLong() + " to " + value);
    }
    takeStoredValueForKey(value, "libelleLong");
  }

  public Long paramId() {
    return (Long) storedValueForKey("paramId");
  }

  public void setParamId(Long value) {
    if (_EOParamListediffusion.LOG.isDebugEnabled()) {
    	_EOParamListediffusion.LOG.debug( "updating paramId from " + paramId() + " to " + value);
    }
    takeStoredValueForKey(value, "paramId");
  }

  public Long persIdCreation() {
    return (Long) storedValueForKey("persIdCreation");
  }

  public void setPersIdCreation(Long value) {
    if (_EOParamListediffusion.LOG.isDebugEnabled()) {
    	_EOParamListediffusion.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "persIdCreation");
  }

  public Long persIdModification() {
    return (Long) storedValueForKey("persIdModification");
  }

  public void setPersIdModification(Long value) {
    if (_EOParamListediffusion.LOG.isDebugEnabled()) {
    	_EOParamListediffusion.LOG.debug( "updating persIdModification from " + persIdModification() + " to " + value);
    }
    takeStoredValueForKey(value, "persIdModification");
  }

  public String valeurDefaut() {
    return (String) storedValueForKey("valeurDefaut");
  }

  public void setValeurDefaut(String value) {
    if (_EOParamListediffusion.LOG.isDebugEnabled()) {
    	_EOParamListediffusion.LOG.debug( "updating valeurDefaut from " + valeurDefaut() + " to " + value);
    }
    takeStoredValueForKey(value, "valeurDefaut");
  }

  public String visibilite() {
    return (String) storedValueForKey("visibilite");
  }

  public void setVisibilite(String value) {
    if (_EOParamListediffusion.LOG.isDebugEnabled()) {
    	_EOParamListediffusion.LOG.debug( "updating visibilite from " + visibilite() + " to " + value);
    }
    takeStoredValueForKey(value, "visibilite");
  }


  public static EOParamListediffusion createParamListediffusion(EOEditingContext editingContext, String libelleCourt
, String libelleLong
, Long paramId
, String valeurDefaut
, String visibilite
) {
    EOParamListediffusion eo = (EOParamListediffusion) EOUtilities.createAndInsertInstance(editingContext, _EOParamListediffusion.ENTITY_NAME);    
		eo.setLibelleCourt(libelleCourt);
		eo.setLibelleLong(libelleLong);
		eo.setParamId(paramId);
		eo.setValeurDefaut(valeurDefaut);
		eo.setVisibilite(visibilite);
    return eo;
  }

  public static NSArray<EOParamListediffusion> fetchAllParamListediffusions(EOEditingContext editingContext) {
    return _EOParamListediffusion.fetchAllParamListediffusions(editingContext, null);
  }

  public static NSArray<EOParamListediffusion> fetchAllParamListediffusions(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOParamListediffusion.fetchParamListediffusions(editingContext, null, sortOrderings);
  }

  public static NSArray<EOParamListediffusion> fetchParamListediffusions(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOParamListediffusion.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOParamListediffusion> eoObjects = (NSArray<EOParamListediffusion>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOParamListediffusion fetchParamListediffusion(EOEditingContext editingContext, String keyName, Object value) {
    return _EOParamListediffusion.fetchParamListediffusion(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOParamListediffusion fetchParamListediffusion(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOParamListediffusion> eoObjects = _EOParamListediffusion.fetchParamListediffusions(editingContext, qualifier, null);
    EOParamListediffusion eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOParamListediffusion)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one ParamListediffusion that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOParamListediffusion fetchRequiredParamListediffusion(EOEditingContext editingContext, String keyName, Object value) {
    return _EOParamListediffusion.fetchRequiredParamListediffusion(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOParamListediffusion fetchRequiredParamListediffusion(EOEditingContext editingContext, EOQualifier qualifier) {
    EOParamListediffusion eoObject = _EOParamListediffusion.fetchParamListediffusion(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no ParamListediffusion that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOParamListediffusion localInstanceIn(EOEditingContext editingContext, EOParamListediffusion eo) {
    EOParamListediffusion localInstance = (eo == null) ? null : (EOParamListediffusion)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
