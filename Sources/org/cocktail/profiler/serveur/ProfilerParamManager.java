package org.cocktail.profiler.serveur;

import org.cocktail.fwkcktldroitsutils.common.metier.EOCompte;
import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlpersonne.common.FwkCktlPersonneParamManager;
import org.cocktail.fwkcktlpersonne.common.PersonneApplicationUser;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametresType;
import org.cocktail.fwkcktlwebapp.server.CktlParamManager;

import com.webobjects.eoaccess.EOEntity;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXEOAccessUtilities;
import er.extensions.foundation.ERXThreadStorage;

public class ProfilerParamManager extends CktlParamManager {
	
	public static final String PROFILER_ADRESSE_PERSO_DESACTIVE = "org.cocktail.profiler.adresse.personaladressmodification.disabled";
	public static final String PROFILER_ADRESSE_READONLY_ACTIVE = "org.cocktail.profiler.adresse.readonly.enabled";
	public static final String PROFILER_TEL_READONLY_ACTIVE = "org.cocktail.profiler.telephone.readonly.enabled";
	public static final String PROFILER_TEL_INT_DESACTIVE = "org.cocktail.profiler.telephone.internetelmodification.disabled";
	public static final String PROFILER_TEL_PRF_DESACTIVE = "org.cocktail.profiler.telephone.protelmodification.disabled";
	public static final String PROFILER_COMPTE_PWD_DESACTIVE = "org.cocktail.profiler.compte.changedpassword.disabled";
	public static final String PROFILER_COMPTE_SECRETQUESTION_DESACTIVE = "org.cocktail.profiler.compte.secretquestion.disabled";
	public static final String PROFILER_PHOTO_READONLY_ACTIVE = "org.cocktail.profiler.photo.readonly.enabled";
	public static final String PROFILER_AGENDA_VISIBLE_ACTIVE = "org.cocktail.profiler.individu.agendavisible.enabled";
	public static final String PROFILER_SCOLARITE_AFFICHAGECUSTOM_ACTIVE = "org.cocktail.profiler.scolarite.affichagecustom.enabled";
	public static final String PROFILER_SCOLARITE_AFFICHAGECOEFFEC_ACTIVE = "org.cocktail.profiler.scolarite.affichagecoeffec.enabled";
	
	public static final String PROFILER_ONGLET_CIR_ACTIVE = "org.cocktail.profiler.onglet.cir.enabled";
	
	public static final String PROFILER_COMPTE_AFFICHAGE_RESTREINT_ACTIVE = "org.cocktail.profiler.compte.affichagerestreint.enabled";
	public static final String PROFILER_PHOTO_ONLYPUBLICATED_ACTIVE = "org.cocktail.profiler.photo.onlypublicated.enabled";

	public static final String PROFILER_ONGLET_ANNUAIRE_ACTIVE = "org.cocktail.profiler.onglet.annuaire.enabled";
	public static final String PROFILER_ANNUAIRE_ACCES_ETUDIANT_ACTIVE = "org.cocktail.profiler.annuaire.acces.etudiant.enabled";
	
	public static final String PROFILER_ONGLET_AUTRES_ACTIVE = "org.cocktail.profiler.onglet.autres.enabled";
	public static final String PROFILER_ONGLET_COMPTES_ACTIVE = "org.cocktail.profiler.onglet.comptes.enabled";

	public static final String PROFILER_BUREAU_READONLY_ACTIVE = "org.cocktail.profiler.bureau.readonly.enabled";
	public static final String PROFILER_BUREAU_AFFICHAGE_ACTIVE = "org.cocktail.profiler.bureau.affichage.enabled";

	public static final String PROFILER_CPTE_STOCKAGE_CLAIR_ACTIVE = "org.cocktail.profiler.compte.stockageenclair.enabled";
	public static final String PROFILER_TEL_LISTE_ROUGE_BLOCAGE_ACTIVE = "org.cocktail.profiler.telephone.listerouge.blocage.enabled";
	
	public static final String PROFILER_CSS = "org.cocktail.profiler.css";
	
	public static final String PROFILER_RESULTAT_RECHERCHE_LIMITE = "org.cocktail.profiler.recherche.max";
	
	public static final String PROFILER_ONGLET_INFO_ADMINISTRATIVE_ACTIVE = "org.cocktail.profiler.onglet.infosadministratives.enabled";
	public static final String PROFILER_ONGLET_INFO_PRO_ACTIVE = "org.cocktail.profiler.onglet.infospro.enabled";
	public static final String PROFILER_ONGLET_SCOL_ACTIVE = "org.cocktail.profiler.onglet.scolarite.enabled";
	
	public static final String PROFILER_ADRESSE_AFFICHAGE_ACTIVE = "org.cocktail.profiler.adresse.affichage.enabled";
	public static final String PROFILER_PHOTO_AFFICHAGE_ACTIVE = "org.cocktail.profiler.photo.affichage.enabled";
	public static final String PROFILER_TELEPHONE_AFFICHAGE_ACTIVE = "org.cocktail.profiler.telephone.affichage.enabled";
	
	public static final String PROFILER_SEND_EMAIL_RH = "org.cocktail.profiler.mail.gerantInfosAdministratives";
	public static final String PROFILER_SEND_EMAIL_SCOL = "org.cocktail.profiler.mail.gerantInfosScolarite";
	
	public static final String PROFILER_BDD_VIEW_INACTIVE = "org.cocktail.profiler.bdd.view.disabled";
	
	public static final String PROFILER_EMAIL_RH_DEFAULT_VALUE = "rh@univ.fr, rh2@univ.fr";
	public static final String PROFILER_EMAIL_SCOL_DEFAULT_VALUE = "scol@univ.fr";
	
	public static final String PROFILER_ADRESSE_SUPPRESSION_ACTIF = "org.cocktail.profiler.adresse.suppression.enabled";
	public static final String PROFILER_ADRESSE_AJOUT_ACTIF = "org.cocktail.profiler.adresse.ajout.enabled";
	

	private EOEditingContext ec = ERXEC.newEditingContext();
	
	public ProfilerParamManager() {
		getParamList().add(PROFILER_ADRESSE_PERSO_DESACTIVE);
		getParamComments().put(PROFILER_ADRESSE_PERSO_DESACTIVE, "Autoriser ou non un individu à modifier son adresse personnelle");
		getParamDefault().put(PROFILER_ADRESSE_PERSO_DESACTIVE, "N");
		getParamTypes().put(PROFILER_ADRESSE_PERSO_DESACTIVE, EOGrhumParametresType.codeActivation);
		
		getParamList().add(PROFILER_TEL_INT_DESACTIVE);
		getParamComments().put(PROFILER_TEL_INT_DESACTIVE, "Autoriser ou non un individu à modifier son numéro de téléphone interne");
		getParamDefault().put(PROFILER_TEL_INT_DESACTIVE, "N");
		getParamTypes().put(PROFILER_TEL_INT_DESACTIVE, EOGrhumParametresType.codeActivation);
		
		getParamList().add(PROFILER_TEL_PRF_DESACTIVE);
		getParamComments().put(PROFILER_TEL_PRF_DESACTIVE, "Autoriser ou non un individu à modifier son numéro de téléphone interne");
		getParamDefault().put(PROFILER_TEL_PRF_DESACTIVE, "N");
		getParamTypes().put(PROFILER_TEL_PRF_DESACTIVE, EOGrhumParametresType.codeActivation);
		
		getParamList().add(PROFILER_COMPTE_PWD_DESACTIVE);
		getParamComments().put(PROFILER_COMPTE_PWD_DESACTIVE, "Autoriser ou non un individu à modifier son password");
		getParamDefault().put(PROFILER_COMPTE_PWD_DESACTIVE, "N");
		getParamTypes().put(PROFILER_COMPTE_PWD_DESACTIVE, EOGrhumParametresType.codeActivation);

		getParamList().add(PROFILER_COMPTE_SECRETQUESTION_DESACTIVE);
		getParamComments().put(PROFILER_COMPTE_SECRETQUESTION_DESACTIVE, "Autoriser ou non un individu à accéder à la question secrète");
		getParamDefault().put(PROFILER_COMPTE_SECRETQUESTION_DESACTIVE, "N");
		getParamTypes().put(PROFILER_COMPTE_SECRETQUESTION_DESACTIVE, EOGrhumParametresType.codeActivation);

		getParamList().add(PROFILER_PHOTO_READONLY_ACTIVE);
		getParamComments().put(PROFILER_PHOTO_READONLY_ACTIVE, "Mettre ou non la photo d'un individu en lecture seulement");
		getParamDefault().put(PROFILER_PHOTO_READONLY_ACTIVE, "N");
		getParamTypes().put(PROFILER_PHOTO_READONLY_ACTIVE, EOGrhumParametresType.codeActivation);

		getParamList().add(PROFILER_ADRESSE_READONLY_ACTIVE);
		getParamComments().put(PROFILER_ADRESSE_READONLY_ACTIVE, "Mettre ou non la ou les adresses d'un individu en lecture seulement");
		getParamDefault().put(PROFILER_ADRESSE_READONLY_ACTIVE, "N");
		getParamTypes().put(PROFILER_ADRESSE_READONLY_ACTIVE, EOGrhumParametresType.codeActivation);

		getParamList().add(PROFILER_TEL_READONLY_ACTIVE);
		getParamComments().put(PROFILER_TEL_READONLY_ACTIVE, "Mettre ou non le ou les téléphones d'un individu en lecture seulement");
		getParamDefault().put(PROFILER_TEL_READONLY_ACTIVE, "N");
		getParamTypes().put(PROFILER_TEL_READONLY_ACTIVE, EOGrhumParametresType.codeActivation);

		getParamList().add(PROFILER_AGENDA_VISIBLE_ACTIVE);
		getParamComments().put(PROFILER_AGENDA_VISIBLE_ACTIVE, "Activer ou désactiver la visibilité des agendas d'un individu");
		getParamDefault().put(PROFILER_AGENDA_VISIBLE_ACTIVE, "O");
		getParamTypes().put(PROFILER_AGENDA_VISIBLE_ACTIVE, EOGrhumParametresType.codeActivation);
		
		getParamList().add(PROFILER_SCOLARITE_AFFICHAGECUSTOM_ACTIVE);
		getParamComments().put(PROFILER_SCOLARITE_AFFICHAGECUSTOM_ACTIVE, "Activer ou désactiver l'affichage customisé de la Scolarité (Défaut : N)");
		getParamDefault().put(PROFILER_SCOLARITE_AFFICHAGECUSTOM_ACTIVE, "N");
		getParamTypes().put(PROFILER_SCOLARITE_AFFICHAGECUSTOM_ACTIVE, EOGrhumParametresType.codeActivation);

		getParamList().add(PROFILER_SCOLARITE_AFFICHAGECOEFFEC_ACTIVE);
		getParamComments().put(PROFILER_SCOLARITE_AFFICHAGECOEFFEC_ACTIVE, "Affichage du coefficient EC (Défaut : Y) ou du coefficient examen dans l'affichage customisé de la Scolarité ");
		getParamDefault().put(PROFILER_SCOLARITE_AFFICHAGECOEFFEC_ACTIVE, "Y");
		getParamTypes().put(PROFILER_SCOLARITE_AFFICHAGECOEFFEC_ACTIVE, EOGrhumParametresType.codeActivation);
		

		getParamList().add(PROFILER_CSS);
		getParamComments().put(PROFILER_CSS, "Choix du CSS d'habillage en Orange, Vert ou Bleu (par défaut : CktlCommonOrange.css ");
		getParamDefault().put(PROFILER_CSS, "CktlCommonOrange.css");
		getParamTypes().put(PROFILER_CSS, EOGrhumParametresType.texteLibre);
		
		
		
		
		

		getParamList().add(PROFILER_ADRESSE_AFFICHAGE_ACTIVE);
		getParamComments().put(PROFILER_ADRESSE_AFFICHAGE_ACTIVE, "Activer ou désactiver l'affichage du cadre Adresse (Défaut : O)");
		getParamDefault().put(PROFILER_ADRESSE_AFFICHAGE_ACTIVE, "O");
		getParamTypes().put(PROFILER_ADRESSE_AFFICHAGE_ACTIVE, EOGrhumParametresType.codeActivation);
		
		getParamList().add(PROFILER_BUREAU_AFFICHAGE_ACTIVE);
		getParamComments().put(PROFILER_BUREAU_AFFICHAGE_ACTIVE, "Activer ou désactiver l'affichage de la localisation d'un individu (Défaut : N)");
		getParamDefault().put(PROFILER_BUREAU_AFFICHAGE_ACTIVE, "N");
		getParamTypes().put(PROFILER_BUREAU_AFFICHAGE_ACTIVE, EOGrhumParametresType.codeActivation);
		
		getParamList().add(PROFILER_PHOTO_AFFICHAGE_ACTIVE);
		getParamComments().put(PROFILER_PHOTO_AFFICHAGE_ACTIVE, "Activer ou désactiver l'affichage de la photo d'un individu (Défaut : O)");
		getParamDefault().put(PROFILER_PHOTO_AFFICHAGE_ACTIVE, "O");
		getParamTypes().put(PROFILER_PHOTO_AFFICHAGE_ACTIVE, EOGrhumParametresType.codeActivation);
		
		getParamList().add(PROFILER_TELEPHONE_AFFICHAGE_ACTIVE);
		getParamComments().put(PROFILER_TELEPHONE_AFFICHAGE_ACTIVE, "Activer ou désactiver l'affichage du cadre Téléphones (Défaut : O)");
		getParamDefault().put(PROFILER_TELEPHONE_AFFICHAGE_ACTIVE, "O");
		getParamTypes().put(PROFILER_TELEPHONE_AFFICHAGE_ACTIVE, EOGrhumParametresType.codeActivation);
		

		getParamList().add(PROFILER_BUREAU_READONLY_ACTIVE);
		getParamComments().put(PROFILER_BUREAU_READONLY_ACTIVE, "Activer ou désactiver l'affichage en mode 'Read Only' de la localisation d'un individu (Défaut : N)");
		getParamDefault().put(PROFILER_BUREAU_READONLY_ACTIVE, "N");
		getParamTypes().put(PROFILER_BUREAU_READONLY_ACTIVE, EOGrhumParametresType.codeActivation);
		
		
		getParamList().add(PROFILER_CPTE_STOCKAGE_CLAIR_ACTIVE);
		getParamComments().put(PROFILER_CPTE_STOCKAGE_CLAIR_ACTIVE, "Activer ou désactiver le stockage en clair du mot de passe (Défaut : N)");
		getParamDefault().put(PROFILER_CPTE_STOCKAGE_CLAIR_ACTIVE, "N");
		getParamTypes().put(PROFILER_CPTE_STOCKAGE_CLAIR_ACTIVE, EOGrhumParametresType.codeActivation);
		
		
		getParamList().add(PROFILER_ONGLET_ANNUAIRE_ACTIVE);
		getParamComments().put(PROFILER_ONGLET_ANNUAIRE_ACTIVE, "Activer ou désactiver la visibilité de l'onglet Annuaire (Défaut : O)");
		getParamDefault().put(PROFILER_ONGLET_ANNUAIRE_ACTIVE, "O");
		getParamTypes().put(PROFILER_ONGLET_ANNUAIRE_ACTIVE, EOGrhumParametresType.codeActivation);
		
		
		getParamList().add(PROFILER_ONGLET_AUTRES_ACTIVE);
		getParamComments().put(PROFILER_ONGLET_AUTRES_ACTIVE, "Activer ou désactiver la visibilité de l'onglet Autres (Défaut : N)");
		getParamDefault().put(PROFILER_ONGLET_AUTRES_ACTIVE, "N");
		getParamTypes().put(PROFILER_ONGLET_AUTRES_ACTIVE, EOGrhumParametresType.codeActivation);
		
		
		getParamList().add(PROFILER_ONGLET_COMPTES_ACTIVE);
		getParamComments().put(PROFILER_ONGLET_COMPTES_ACTIVE, "Activer ou désactiver la visibilité de l'onglet Comptes (Défaut : O)");
		getParamDefault().put(PROFILER_ONGLET_COMPTES_ACTIVE, "O");
		getParamTypes().put(PROFILER_ONGLET_COMPTES_ACTIVE, EOGrhumParametresType.codeActivation);
		
		
		getParamList().add(PROFILER_ONGLET_INFO_ADMINISTRATIVE_ACTIVE);
		getParamComments().put(PROFILER_ONGLET_INFO_ADMINISTRATIVE_ACTIVE, "Activer ou désactiver la visibilité de l'onglet Infos Administratives (Défaut : O)");
		getParamDefault().put(PROFILER_ONGLET_INFO_ADMINISTRATIVE_ACTIVE, "O");
		getParamTypes().put(PROFILER_ONGLET_INFO_ADMINISTRATIVE_ACTIVE, EOGrhumParametresType.codeActivation);
		
		
		getParamList().add(PROFILER_ONGLET_INFO_PRO_ACTIVE);
		getParamComments().put(PROFILER_ONGLET_INFO_PRO_ACTIVE, "Activer ou désactiver la visibilité de l'onglet Infos Pro (Défaut : O)");
		getParamDefault().put(PROFILER_ONGLET_INFO_PRO_ACTIVE, "O");
		getParamTypes().put(PROFILER_ONGLET_INFO_PRO_ACTIVE, EOGrhumParametresType.codeActivation);
		
		
		getParamList().add(PROFILER_ONGLET_SCOL_ACTIVE);
		getParamComments().put(PROFILER_ONGLET_SCOL_ACTIVE, "Activer ou désactiver la visibilité de l'onglet Scolarité (Défaut : O)");
		getParamDefault().put(PROFILER_ONGLET_SCOL_ACTIVE, "O");
		getParamTypes().put(PROFILER_ONGLET_SCOL_ACTIVE, EOGrhumParametresType.codeActivation);
		
		
		
		
		
		
		
		getParamList().add(PROFILER_ANNUAIRE_ACCES_ETUDIANT_ACTIVE);
		getParamComments().put(PROFILER_ANNUAIRE_ACCES_ETUDIANT_ACTIVE, "Activer ou désactiver l'accès de l'Annuaire aux étudiants (Défaut : N)");
		getParamDefault().put(PROFILER_ANNUAIRE_ACCES_ETUDIANT_ACTIVE, "N");
		getParamTypes().put(PROFILER_ANNUAIRE_ACCES_ETUDIANT_ACTIVE, EOGrhumParametresType.codeActivation);
		
		
		getParamList().add(PROFILER_TEL_LISTE_ROUGE_BLOCAGE_ACTIVE);
		getParamComments().put(PROFILER_TEL_LISTE_ROUGE_BLOCAGE_ACTIVE, "Activer ou désactiver le blocage sur la visualisation des Tél de la liste rouge (Défaut : N)");
		getParamDefault().put(PROFILER_TEL_LISTE_ROUGE_BLOCAGE_ACTIVE, "N");
		getParamTypes().put(PROFILER_TEL_LISTE_ROUGE_BLOCAGE_ACTIVE, EOGrhumParametresType.codeActivation);
		
		getParamList().add(PROFILER_COMPTE_AFFICHAGE_RESTREINT_ACTIVE);
		getParamComments().put(PROFILER_COMPTE_AFFICHAGE_RESTREINT_ACTIVE, "Activer/Désactiver l'affichage restreint du compte (Défaut : N).");
		getParamDefault().put(PROFILER_COMPTE_AFFICHAGE_RESTREINT_ACTIVE, "N");
		getParamTypes().put(PROFILER_COMPTE_AFFICHAGE_RESTREINT_ACTIVE, EOGrhumParametresType.codeActivation);

		getParamList().add(PROFILER_PHOTO_ONLYPUBLICATED_ACTIVE);
		getParamComments().put(PROFILER_PHOTO_ONLYPUBLICATED_ACTIVE, "Activer/Désactiver seulement les boutons de publication (Défaut : N).");
		getParamDefault().put(PROFILER_PHOTO_ONLYPUBLICATED_ACTIVE, "N");
		getParamTypes().put(PROFILER_PHOTO_ONLYPUBLICATED_ACTIVE, EOGrhumParametresType.codeActivation);
		

		getParamList().add(PROFILER_RESULTAT_RECHERCHE_LIMITE);
		getParamComments().put(PROFILER_RESULTAT_RECHERCHE_LIMITE, "Nombre maximal de recherche effectuée dans l'application Profiler (Défaut : 40).");
		getParamDefault().put(PROFILER_RESULTAT_RECHERCHE_LIMITE, "40");
		getParamTypes().put(PROFILER_RESULTAT_RECHERCHE_LIMITE, EOGrhumParametresType.valeurNumerique);
		
		
		
		getParamList().add(PROFILER_BDD_VIEW_INACTIVE);
		getParamComments().put(PROFILER_BDD_VIEW_INACTIVE, "Cacher ou non l'affichage de la BDD (Défaut : Y)");
		getParamDefault().put(PROFILER_BDD_VIEW_INACTIVE, "Y");
		getParamTypes().put(PROFILER_BDD_VIEW_INACTIVE, EOGrhumParametresType.codeActivation);
		
		

		getParamList().add(PROFILER_SEND_EMAIL_RH);
		getParamComments().put(PROFILER_SEND_EMAIL_RH, "Mail(s) de la (des) personne(s) gérant les informations administratives (séparés par une virgule si besoin); (Défaut : une chaîne  à changer qui est rh@univ.fr, rh2@univ.fr si vous souhaitez préciser un email).");
		getParamDefault().put(PROFILER_SEND_EMAIL_RH, "rh@univ.fr, rh2@univ.fr");
		getParamTypes().put(PROFILER_SEND_EMAIL_RH, EOGrhumParametresType.texteLibre);

		getParamList().add(PROFILER_SEND_EMAIL_SCOL);
		getParamComments().put(PROFILER_SEND_EMAIL_SCOL, "Mail(s) de la (des) personne(s) gérant les informations de la scolarite (séparés par une virgule si besoin); (Défaut : une chaîne  à changer qui est scol@univ.fr si vous souhaitez préciser un email).");
		getParamDefault().put(PROFILER_SEND_EMAIL_SCOL, "scol@univ.fr");
		getParamTypes().put(PROFILER_SEND_EMAIL_SCOL, EOGrhumParametresType.texteLibre);

		
		getParamList().add(PROFILER_ADRESSE_SUPPRESSION_ACTIF);
		getParamComments().put(PROFILER_ADRESSE_SUPPRESSION_ACTIF, "Affichage ou non du bouton de suppresion d'une adresse (Défaut : Y)");
		getParamDefault().put(PROFILER_ADRESSE_SUPPRESSION_ACTIF, "Y");
		getParamTypes().put(PROFILER_ADRESSE_SUPPRESSION_ACTIF, EOGrhumParametresType.codeActivation);
		
		getParamList().add(PROFILER_ADRESSE_AJOUT_ACTIF);
		getParamComments().put(PROFILER_ADRESSE_AJOUT_ACTIF, "Affichage ou non du bouton d'ajout d'une adresse (Défaut : N)");
		getParamDefault().put(PROFILER_ADRESSE_AJOUT_ACTIF, "N");
		getParamTypes().put(PROFILER_ADRESSE_AJOUT_ACTIF, EOGrhumParametresType.codeActivation);
		
	}
	
	@Override
	public void createNewParam(String key, String value, String comment) {
		createNewParam(key, value, comment, EOGrhumParametresType.codeActivation);
	}
	
	@Override
	public void checkAndInitParamsWithDefault() {
		//Recuperer un grhum_createur
		String cptLogin = EOGrhumParametres.parametrePourCle(ec, EOGrhumParametres.PARAM_GRHUM_CREATEUR);
		if (cptLogin != null) {
			EOCompte cpt = EOCompte.compteForLogin(ec, cptLogin);
			if (cpt != null) {
				ERXThreadStorage.takeValueForKey(cpt.persId(), PersonneApplicationUser.PERS_ID_CURRENT_USER_STORAGE_KEY);
			}
		}
		super.checkAndInitParamsWithDefault();
	}

	@Override
	public void createNewParam(String key, String value, String comment,
			String type) {
		EOGrhumParametres newParametre = EOGrhumParametres.creerInstance(ec);
		newParametre.setParamKey(key);
		newParametre.setParamValue(value);
		newParametre.setParamCommentaires(comment);
		newParametre.setToParametresTypeRelationship(EOGrhumParametresType.fetchByKeyValue(ec, EOGrhumParametresType.TYPE_ID_INTERNE_KEY, type));
		if (ec.hasChanges()) {
			EOEntity entityParameter = ERXEOAccessUtilities.entityForEo(newParametre);
			try {

				// Avant de sauvegarder les données, nous modifions le modèle
				// pour que l'on puisse avoir accès aussi en écriture sur les données
				entityParameter.setReadOnly(false);
				ec.saveChanges();

			} catch (Exception e) {
				log.warn("Erreur lors de l'enregistrement des parametres.");
				e.printStackTrace();
			} finally {
				entityParameter.setReadOnly(true);
			}
		}
	}

	@Override
	public String getParam(String key) {
		String res = getApplication().config().stringForKey(key);
		//		String res = EOGrhumParametres.parametrePourCle(ec, key);
		return res;
	}

}
