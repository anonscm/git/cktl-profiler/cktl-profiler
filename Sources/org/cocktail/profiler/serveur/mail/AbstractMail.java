package org.cocktail.profiler.serveur.mail;

import javax.mail.MessagingException;

import org.apache.commons.lang.text.StrSubstitutor;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.FwkCktlPersonne;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.profiler.serveur.ProfilerParamManager;

import com.webobjects.foundation.NSArray;

import er.extensions.foundation.ERXProperties;
import er.javamail.ERMailDeliveryPlainText;

/**
 * Classe abstraite utilitaire permettant le formattage et l'envoi d'e-mail.
 */
public abstract class AbstractMail {

	private static Logger logger = Logger.getLogger(AbstractMail.class);
	private static NSArray<String> tableauVide = new NSArray<String>();
	
	public static final String EMAIL_ACTIF = "org.cocktail.profiler.email.actif";

	
	
	/**
	 * Est-ce que l'envoi de mail est actif ? (valeur du paramètre {@value #EMAIL_ACTIF})
	 * @return <code>true</code> si actif
	 */
	public boolean getActif() {
		return ERXProperties.booleanForKeyWithDefault(EMAIL_ACTIF, false);
	}
	
	
	/**
	 * 
	 * @return true si la valeur par défaut a été changée
	 */
	public boolean hasEmailRH() {
		if (!ProfilerParamManager.PROFILER_EMAIL_RH_DEFAULT_VALUE.equals(getEmailRH())) {
			return true;
		}
		return false;
	}
	
	/**
	 * @return adresse mail de la RH
	 */
	public String getEmailRH() {
		return FwkCktlPersonne.paramManager.getParam(ProfilerParamManager.PROFILER_SEND_EMAIL_RH);
	}
	
	/**
	 * Retourne les paramètres variables du message pouvant être substitués (champs/valeur).
	 * <p>
	 * Surcharger cette méthode si vous avez des champs variables dans votre e-mail (sujet et/ou corps).
	 * <p>
	 * L'implémentation classique est celle-ci :
	 * <pre>
	 *   Map<String, Object> valuesMap = new HashMap<String, Object>();
	 *   valuesMap.put("prenom", etudiant.prenomAffichage());
	 *   valuesMap.put("nom", etudiant.nomAffichage());
	 *   
	 *   StrSubstitutor strSubtitutor = new StrSubstitutor(valuesMap);
	 *   
	 *   return strSubtitutor;
	 * </pre>
	 * 
	 * @return les paramètres variables du message pouvant être substitués
	 */
	protected StrSubstitutor getStrSubstitutor() {
		return null;
	}
	
	/**
	 * Retourne l'adresse 'From' de l'expéditeur.
	 * <p>
	 * Par défaut retourne la valeur du paramètre {@value #EMAIL_EXPEDITEUR}.
	 * <p>
	 * Vous pouvez surcharger cette méthode.
	 * 
	 * @return l'adresse 'From' de l'expéditeur
	 */
	protected String getFromAdresse() {
		return "noReply@" + FwkCktlPersonne.paramManager.getParam(EOGrhumParametres.PARAM_GRHUM_DOMAINE_PRINCIPAL);
	}
	
	/**
	 * Retourne les adresses 'To' du ou des destinataires.
	 * @return les adresses 'To' du ou des destinataires
	 */
	protected abstract NSArray<String> getToAdresses();
	
	/**
	 * Retourne les adresses 'CC' (copie-carbone) du ou des destinataires.
	 * @return les adresses 'CC' (copie-carbone) du ou des destinataires
	 */
	protected NSArray<String> getCCAdresses() {
		return tableauVide;
	}

	/**
	 * Retourne les adresses 'BCC' (copie-carbone cachée) du ou des destinataires.
	 * @return les adresses 'BCC' (copie-carbone cachée) du ou des destinataires
	 */
	protected NSArray<String> getBCCAdresses() {
		return tableauVide;
	}

	/**
	 * Retourne le template utilisé pour générer le sujet de l'e-mail.
	 * @return le template utilisé pour générer le sujet de l'e-mail
	 */
	protected abstract String getTemplateSujet();
	
	/**
	 * Retourne le template utilisé pour générer le corps de l'e-mail.
	 * @return le template utilisé pour générer le corps de l'e-mail
	 */
	protected abstract String getTemplateCorps(boolean isAdresseModifiee, boolean isTelephoneModifiee);
	
	/**
	 * Retourne le sujet de l'e-mail formatté.
	 * @param strSubtitutor les valeurs de substitution
	 * @return le sujet formatté
	 */
	protected String getSujet(StrSubstitutor strSubtitutor) {
		if (strSubtitutor == null) {
			return getTemplateSujet();
		}
		
		return strSubtitutor.replace(getTemplateSujet());
	}
	
	/**
	 * Retourne le corps de l'e-mail formatté.
	 * @param strSubtitutor les valeurs de substitution
	 * @return le corps formatté
	 */
	protected String getCorps(StrSubstitutor strSubtitutor, boolean isAdresseModifiee, boolean isTelephoneModifiee) {
		if (strSubtitutor == null) {
			return getTemplateCorps(isAdresseModifiee, isTelephoneModifiee);
		}
		
		return strSubtitutor.replace(getTemplateCorps(isAdresseModifiee, isTelephoneModifiee));
	}
	
	/**
	 * Formatter et envoyer l'e-mail.
	 * @return <code>true</code> si l'e-mail a été envoyé.
	 */
	public boolean envoyerMail(boolean isAdresseModifiee, boolean isTelephoneModifiee) {
		boolean estEnvoye = false;
		
		boolean envoiMailActif = (hasEmailRH() &&  getActif());
		String from = getFromAdresse();
		
		if (!isAdresseModifiee && !isTelephoneModifiee) {
			return estEnvoye;
		}
		
		if (!envoiMailActif) {
			logger.info("L'envoi de mail est désactivé (" + ProfilerParamManager.PROFILER_SEND_EMAIL_RH + " est à la valeur par défaut donc pas d'envoi).");
		} else {
			NSArray<String> tos = getToAdresses();
			
			if (tos.isEmpty()) {
				logger.error("Pas d'adresse de destinataire. Mail non envoyé.");
			} else {
				StrSubstitutor strSubstitutor = getStrSubstitutor();
				String sujet = getSujet(null);
				String corps = null;
				if (isAdresseModifiee && isTelephoneModifiee) {
					corps = getCorps(strSubstitutor, true, true);
				}
				if (isAdresseModifiee && !isTelephoneModifiee) {
					corps = getCorps(strSubstitutor, true, false);
				}
				if (!isAdresseModifiee && isTelephoneModifiee) {
					corps = getCorps(strSubstitutor, false, true);
				}
				NSArray<String> ccs = getCCAdresses();
				NSArray<String> bccs = getBCCAdresses();
				
				try {
					ERMailDeliveryPlainText mailDelivery = new ERMailDeliveryPlainText();
					
					mailDelivery.setSubject(sujet);
					mailDelivery.setTextContent(corps);
					mailDelivery.setFromAddress(from);
					mailDelivery.setToAddresses(tos);
					mailDelivery.setCCAddresses(ccs);
					mailDelivery.setBCCAddresses(bccs);
					mailDelivery.sendMail();
					
					estEnvoye = true;
				} catch (MessagingException e) {
					logger.error("Une erreur est survenue lors de l'envoi du mail");
					logger.error(e);
				}
			}
		}
		
		return estEnvoye;
	}
}
