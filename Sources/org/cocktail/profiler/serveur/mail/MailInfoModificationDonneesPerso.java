package org.cocktail.profiler.serveur.mail;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.text.StrSubstitutor;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.foundation.ERXArrayUtilities;
import er.extensions.localization.ERXLocalizer;

/**
 * Cette classe représente l'e-mail envoyé au service RH lorsque l'individu modifie son adresse et/ou son téléphone
 */
public class MailInfoModificationDonneesPerso extends AbstractMail {

	private static Logger logger = Logger.getLogger(MailInfoModificationDonneesPerso.class);

	private static final String SUJET_MAIL_TEMPLATE = "EmailModification.CourrielSujetTemplate";
	private static final String CORPS_MAIL_ADRESSE_TEMPLATE = "EmailModification.CourrielCorpsTemplateAdresse";
	private static final String CORPS_MAIL_TELEPHONE_TEMPLATE = "EmailModification.CourrielCorpsTemplateTelephone";
	private static final String CORPS_MAIL_ADRESSE_TEL_TEMPLATE = "EmailModification.CourrielCorpsTemplateAdresseTel";

	private ERXLocalizer localizer;
	private IIndividu individu;

	/**
	 * Constructeur.
	 * 
	 * @param localizer fichier de langue
	 * @param individu l'étudiant destinataire du mail
	 */
	public MailInfoModificationDonneesPerso(ERXLocalizer localizer, IIndividu individu) {
		this.localizer = localizer;
		this.individu = individu;
	}


	public IIndividu getIndividu() {
		return individu;
	}


	public void setIndividu(IIndividu individu) {
		this.individu = individu;
	}


	protected String getTemplateSujet() {
		return localizer.localizedStringForKey(SUJET_MAIL_TEMPLATE);
	}

	protected String getTemplateCorps(boolean isAdresseModifiee, boolean isTelephoneModifiee) {
		String templateCorps = "";
		if (isAdresseModifiee && isTelephoneModifiee) {
			templateCorps = localizer.localizedStringForKey(CORPS_MAIL_ADRESSE_TEL_TEMPLATE);
		}
		if (isAdresseModifiee && !isTelephoneModifiee) {
			templateCorps = localizer.localizedStringForKey(CORPS_MAIL_ADRESSE_TEMPLATE);
		}
		if (!isAdresseModifiee && isTelephoneModifiee) {
			templateCorps = localizer.localizedStringForKey(CORPS_MAIL_TELEPHONE_TEMPLATE);
		}
		return templateCorps;
	}

	protected StrSubstitutor getStrSubstitutor() {
		Map<String, Object> valuesMap = new HashMap<String, Object>();
		valuesMap.put("prenom", individu.prenomAffichage());
		valuesMap.put("nom", individu.nomAffichage());
		valuesMap.put("persid", individu.persId());

		StrSubstitutor strSubtitutor = new StrSubstitutor(valuesMap);

		return strSubtitutor;
	}

	protected NSArray<String> getToAdresses() {
		NSMutableArray<String> emails = new NSMutableArray<String>();

//		// Récup de l'adresse emails de l'étudiant
//		NSArray<EOPreAdresse> adresseEtudiant = individu.toPreAdresses(EOPreAdresse.TO_TYPE_ADRESSE.dot(EOTypeAdresse.TADR_CODE_KEY).eq(EOTypeAdresse.TADR_CODE_ETUD));
//
//		if (!adresseEtudiant.isEmpty()) {
//			ERXArrayUtilities.safeAddObject(emails, adresseEtudiant.get(0).email());
//		}
		
		if (hasEmailRH()) {
			ERXArrayUtilities.safeAddObject(emails, getEmailRH());
		}

		return emails;
	}
	
	
}
