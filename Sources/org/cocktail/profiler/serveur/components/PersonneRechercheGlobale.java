package org.cocktail.profiler.serveur.components;

//import org.cocktail.agrhum.serveur.components.EditIndividuPage;
//import org.cocktail.agrhum.serveur.components.EditStructurePage;
//import org.cocktail.agrhum.serveur.components.PersonneRechercheGlobale.DgDelegate;
import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.EOCompte;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.profiler.serveur.UtilMessages;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXS;
import er.extensions.eof.ERXSortOrdering;
import er.extensions.foundation.ERXArrayUtilities;
import er.extensions.foundation.ERXStringUtilities;

public class PersonneRechercheGlobale extends ProfilBaseCmp {

	private static final long serialVersionUID = -8648012745496408688L;

	public static final String BINDING_RESULT = "resultRecherche";

	private String srchPatern;
	private IPersonne selectedPersonne;
	private WODisplayGroup displayGroup;

	private boolean isSelectionDone = false;
	private boolean wantReset = false;

    public PersonneRechercheGlobale(WOContext context) {
        super(context);

        setSelectedPersonne(null);
        setSelectionDone(false);
        displayGroup = null;
    }


    public String getComposantContainerId() {
    	return getComponentId() + "_" + "ComposantContainer";
    }

    public String getGlobalSrchContainerId() {
		return getComponentId() + "_" + "GlobalSrchContainer";
	}

    public String getVisualisationContainerId() {
    	return getComponentId() + "_" + "VisualisationContainer";
    }

	public boolean isSelectionDone() {
		return isSelectionDone;
	}

	public void setSelectionDone(boolean isSelectionDone) {
		this.isSelectionDone = isSelectionDone;
	}

	public boolean isWantReset() {
		return wantReset;
	}


	public void setWantReset(boolean wantReset) {
		this.wantReset = wantReset;
	}


	public String srchPaternFieldId() {
		return getComponentId() + "_" + "TFSrchPatern";
	}

	public String getSrchPatern() {
		if (srchPatern != null) {
			srchPatern = srchPatern.trim();
		}
		return srchPatern;
	}

	public void setSrchPatern(String srchPatern) {
		this.srchPatern = srchPatern;
	}

	public String getSrchFormId() {
		return getComponentId() + "_" + "GlobalSrchForm";
	}

	public WOActionResults doGlobalSearch() {
		try {
			setSelectedPersonne(null);

			NSArray res = NSArray.EmptyArray;
			displayGroup().setObjectArray(res);
			res = getResultats();
			displayGroup().setObjectArray(res);
		} catch (Exception e) {
			logger().error(e.getMessage(), e);
			UtilMessages
			.creatMessageUtil(
					session(),
					UtilMessages.ERROR_MESSAGE,
					"La recherche a échoué :\n"
							+ e.getLocalizedMessage());
		}
		return null;
	}

	public String getGlobalSearchResultModalID() {
		return getComponentId() + "_" + "GlobalSrchResultModal";
	}

	public Boolean hasResults() {
		return Boolean.valueOf(displayGroup().allObjects() != null && displayGroup().allObjects().count() > 0);
	}

	public class DgDelegate {
		public void displayGroupDidChangeSelection(WODisplayGroup group) {
			setSelectedPersonne((IPersonne) group.selectedObject());
		}
	}

	public IPersonne getSelectedPersonne() {
		return selectedPersonne;
	}

	public void setSelectedPersonne(IPersonne personne) {
		selectedPersonne = personne;
	}

	public WODisplayGroup displayGroup() {
		if (displayGroup == null) {
			displayGroup = new WODisplayGroup();
		}
		displayGroup.setDelegate(new DgDelegate());
		return displayGroup;
	}

	public Integer utilisateurPersId() {
		return getPuser().getPersId();
	}

	public NSArray getResultats() {

		NSArray<IPersonne> resultats = new NSMutableArray<IPersonne>();
		NSArray<EOIndividu> individus = new NSMutableArray<EOIndividu>();

		String srchPatern = getSrchPatern();

		if (!MyStringCtrl.isEmpty(srchPatern)) {
			srchPatern = MyStringCtrl.replace(srchPatern, " ", "*");
		}


		individus.addAll(EOIndividu.individusWithNameLikeAndFirstNameEquals(edc(), srchPatern, null, application().getSearchFetchLimit()));
		individus.addAll(EOIndividu.individusWithFirstNameLike(edc(), srchPatern, null, application().getSearchFetchLimit()));
		individus.addAll(EOIndividu.individusWithMiddleNameLike(edc(), srchPatern, null, application().getSearchFetchLimit()));
		individus.addAll(EOIndividu.individusWithLoginEquals(edc(), srchPatern, null, application().getSearchFetchLimit()));

		individus = ERXArrayUtilities.arrayWithoutDuplicates(individus);
		
		individus = individusFiltredAsStudentOrPersonal(individus);

		resultats.addAll(individus);
		return ERXSortOrdering.sortedArrayUsingKeyOrderArray(resultats, ERXS.ascInsensitives(IPersonne.NOM_PRENOM_RECHERCHE_KEY));
	}

	public WOActionResults doSelection() {
		WOActionResults nextPage = null;
		setSelectionDone(true);
		CktlAjaxWindow.close(context());
		return nextPage;
	}

	public WOActionResults onCloseResultModal() {
		resetComponent();
        return null;
    }

	protected void resetComponent() {
		setSelectedPersonne(null);
		setSrchPatern(null);
		displayGroup().setObjectArray(NSArray.EmptyArray);
	}

	public String onFormSrchObserverComplete() {
		return "function(){" + "openCAW_" + getGlobalSearchResultModalID() + "();" + "}";

	}

	public String onFormSrchObserverSuccess() {
		return "function(oC){" + "openCAW_" + getGlobalSearchResultModalID() + "();" + "return false;"+ "}";
	}

	public boolean isOnePersonSelected(){
		boolean isOnePersonSelected = getSelectedPersonne() != null;
		return isOnePersonSelected && isSelectionDone();
	}

	public boolean isNobodySelected(){
		return !isOnePersonSelected();
	}

	public WOActionResults onRecommencer() {
		setWantReset(true);
		setSelectedPersonne(null);
		setSelectionDone(false);
		return null;
	}
	
	private NSArray<EOIndividu> individusFiltredAsStudentOrPersonal(NSArray<EOIndividu> individus) {

		NSMutableArray<EOIndividu> resultats = new NSMutableArray<EOIndividu>();

		for (EOIndividu individu : individus) {
			if (isEtabMember(individu)) {
				resultats.add(individu);
			}
		}
		
		return resultats.immutableClone();
	}


	private boolean isEtabMember(EOIndividu individu) {
		return individu.isEnseignant()
				|| individu.isDoctorant()
				|| individu.isEtudiant()
				|| individu.isPersonnel();
	}
}