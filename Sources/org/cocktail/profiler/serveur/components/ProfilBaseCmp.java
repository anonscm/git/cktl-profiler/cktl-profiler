/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.profiler.serveur.components;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlEnvironmentBackground;
import org.cocktail.fwkcktlpersonne.common.FwkCktlPersonne;
import org.cocktail.fwkcktlpersonne.common.metier.EOCompte;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlpersonne.common.metier.PersonneDelegate;
import org.cocktail.fwkcktlpersonneguiajax.serveur.components.AComponent;
import org.cocktail.profiler.serveur.Application;
import org.cocktail.profiler.serveur.ProfilPersonneDelegate;
import org.cocktail.profiler.serveur.ProfilerParamManager;
import org.cocktail.profiler.serveur.ProfilerUser;
import org.cocktail.profiler.serveur.Session;
import org.cocktail.profiler.serveur.mail.MailInfoModificationDonneesPerso;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

import er.extensions.foundation.ERXArrayUtilities;
import er.extensions.localization.ERXLocalizer;

public class ProfilBaseCmp extends AComponent {
	
	private static final long serialVersionUID = 1L;
	public static final String BND_EDITED_PERSONNE = "editedPersonne";
	public static final String BND_EC = "ec";
	private NSArray<String> lstEditZones;
	private IPersonne editedPersonne;
	private Boolean cmpEditing = Boolean.FALSE;
	
	private boolean isAdresseModifiee = false;
	private boolean isTelephoneModifie = false;

	private EOEditingContext ec;

	public ProfilBaseCmp(WOContext context) {
		super(context);
	}

	public NSArray<String> getLstEditZones() {
		return lstEditZones;
	}

	public void setLstEditZones(NSArray<String> lstEditZones) {
		this.lstEditZones = lstEditZones;
	}
	
	public Application application() {
        return (Application) super.application();
    }

	public Session session() {
    	return (Session) super.session();
    }
	
	public Boolean getCmpEditing() {
		if (hasBinding("cmpEditing")) {
			cmpEditing = (Boolean) valueForBinding("cmpEditing");
		}
		return cmpEditing;
	}

	public void setCmpEditing(Boolean cmpEditing) {
		this.cmpEditing = cmpEditing;
		if (canSetValueForBinding("cmpEditing")) {
			setValueForBinding(cmpEditing, "cmpEditing");
		}
	}
	

	public boolean isAdresseModifiee() {
		return isAdresseModifiee;
	}

	public void setIsAdresseModifiee(boolean isAdresseModifiee) {
		this.isAdresseModifiee = isAdresseModifiee;
	}


	public boolean isTelephoneModifie() {
		return isTelephoneModifie;
	}

	public void setTelephoneModifie(boolean isTelephoneModifie) {
		this.isTelephoneModifie = isTelephoneModifie;
	}

	/**
	 * @return the editedPersonne
	 */
	public IPersonne editedPersonne() {
		if (hasBinding(BND_EDITED_PERSONNE)) {
			setEditedPersonne((IPersonne) valueForBinding(BND_EDITED_PERSONNE));
		}
		if (editedPersonne == null) {
			if (((Session) session()).getPUser() != null) {
				setEditedPersonne(PersonneDelegate.fetchPersonneByPersId(getEc(),
						((Session) session()).getPUser().getPersId()));
			}

		}

		return editedPersonne;
	}

	/**
	 * @param editedPersonne
	 *            the editedPersonne to set
	 */
	public void setEditedPersonne(IPersonne editedPersonne) {
		this.editedPersonne = editedPersonne;
		
		if (canSetValueForBinding(BND_EDITED_PERSONNE)) {
			setValueForBinding(editedPersonne, BND_EDITED_PERSONNE);
		}
		
	}

	public EOEditingContext getEc() {
		if (hasBinding(BND_EC)) {
			ec = (EOEditingContext) valueForBinding(BND_EC);
		}
		if (ec == null) {
			ec = session().defaultEditingContext();
		}
		return ec;
	}

	public void setEc(EOEditingContext ec) {
		this.ec = ec;
		if (canSetValueForBinding(BND_EC)) {
			setValueForBinding(ec, BND_EC);
		}
	}

	public WOActionResults cancelEdit() {
		getEc().revert();
		return backFromEdition();
	}

	public WOActionResults backFromEdition() {
		setCmpEditing(Boolean.FALSE);
		return null;
	}

	
	public ERXLocalizer localizer() {
    	return session().localizer();
    }
	
	/**
	 * 
	 * @param key the key to set
	 * @return key
	 */
	public String localisation(String key) {
		return session().localizer().localizedStringForKey(key);
	}
	
	/**
	 * le PUser fait la gestion des droits
	 * 
	 * @return ProfilerUser
	 */
	public ProfilerUser getPuser() {
		return ((Session) session()).getPUser();
	}

	/**
	 * methode a surcharger dans les class descendantes pour gestion des droits
	 * 
	 * @returns
	 */
	public boolean haveRight() {
		return true;
	}

	public WOActionResults save() throws Exception {
		//try {
		if (getEditedIndividu() != null) {
			if (!(getEditedIndividu().getPersonneDelegate() instanceof ProfilPersonneDelegate)) {
				getEditedIndividu().setPersonneDelegate(
						new ProfilPersonneDelegate(editedPersonne(),
								((Session) session()).getPUser()));
			}
		}
		
		MailInfoModificationDonneesPerso mail = new MailInfoModificationDonneesPerso(localizer(), getEditedIndividu());
		boolean estEnvoye = mail.envoyerMail(isAdresseModifiee(), isTelephoneModifie());
		
		session().addSimpleInfoMessage(localisation("application.OK"), localisation("DossierAdminValider.enregistrer"));
		
		if (estEnvoye) {
			if (isAdresseModifiee() && isTelephoneModifie()) {
				session().addSimpleInfoMessage(localisation("application.OK"),
						localisation("EmailModification.AdresseTel"));
			}
			if (isAdresseModifiee() && !isTelephoneModifie()) {
				session().addSimpleInfoMessage(localisation("application.OK"),
						localisation("EmailModification.Adresse"));
			}
			if (!isAdresseModifiee() && isTelephoneModifie()) {
				session().addSimpleInfoMessage(localisation("application.OK"),
						localisation("EmailModification.Telephone"));
			}
		}
		
		editedPersonne().willChange();
		edc().saveChanges();
		if (edc().parentObjectStore() instanceof EOEditingContext) {
			((EOEditingContext) edc().parentObjectStore()).saveChanges();
		}
		backFromEdition();
		/*	
		} catch (Exception e) {
				e.printStackTrace();
				throw e;
			}
			//*/
		return null;
	}

//	public Boolean haveModifications() {
//		return getEc().hasChanges();
//	}

	public EOIndividu getEditedIndividu() {
		if (editedPersonne().isIndividu()) {
			return (EOIndividu) editedPersonne();
		}
		return null;
	}
	
	public Logger logger() {
		return application().logger();
	}
	
	/**
	 * @return true si l'onglet Informations Personelles est modifiable, false sinon
	 */
	public boolean isInfosModifiables() {
		if (((Application) application()).config().booleanForKey(ProfilerParamManager.PROFILER_ADRESSE_READONLY_ACTIVE)
				&& ((Application) application()).config().booleanForKey(ProfilerParamManager.PROFILER_TEL_READONLY_ACTIVE)) {
			return false;
		}
		return true;
	}
	
	/**
	 * @return email de la personne connectée
	 */
	public String getEmailApplicationUser() {
		EOCompte compte = ERXArrayUtilities.firstObject(editedPersonne().toComptes(null));
		if (compte != null && compte.toIndividu() != null && compte.toVlans() != null) {
			return compte.toCompteEmail().getEmailFormatte();
		}
		return "";
	}
	
	/**
	 * @return adresse mail de la RH
	 */
	public String getEmailRH() {
//		return ERXProperties.stringForKey("org.cocktail.profiler.mail.gerantInfosAdministratives");
		return FwkCktlPersonne.paramManager.getParam(ProfilerParamManager.PROFILER_SEND_EMAIL_RH);
	}
	
	/**
	 * 
	 * @return true si la valeur par défaut a été changée
	 */
	public boolean hasEmailRH() {
		if (!ProfilerParamManager.PROFILER_EMAIL_RH_DEFAULT_VALUE.equals(getEmailRH())) {
			return true;
		}
		return false;
	}
	
	/**
	 * @return adresse mail de la scolarité
	 */
	public String getEmailScol() {
//		return ERXProperties.stringForKey("org.cocktail.profiler.mail.gerantInfosScolarite");
		return FwkCktlPersonne.paramManager.getParam(ProfilerParamManager.PROFILER_SEND_EMAIL_SCOL);
	}

	/**
	 * 
	 * @return true si la valeur par défaut a été changée
	 */
	public boolean hasEmailScol() {
		if (!ProfilerParamManager.PROFILER_EMAIL_SCOL_DEFAULT_VALUE.equals(getEmailRH())) {
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @return true dans le cas des étudiants
	 */
	public boolean isDependsOnScol() {
		if (getEditedIndividu().isEtudiant() && !getEditedIndividu().isPersonnel()) {
			return true;
		}
		return false;
	}
	
	@Inject
	private CktlEnvironmentBackground cktlEnvironmentBackground;
	 
	public void setCktlEnvironmentBackground(CktlEnvironmentBackground cktlEnvironmentBackground) {
		this.cktlEnvironmentBackground = cktlEnvironmentBackground;
	}
	 
	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		super.appendToResponse(response, context);
		cktlEnvironmentBackground.add();
	}
	
}
