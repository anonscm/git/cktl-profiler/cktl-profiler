package org.cocktail.profiler.serveur.components;

import org.cocktail.fwkcktlgrh.common.metier.EOElements;
import org.cocktail.fwkcktlpersonneguiajax.serveur.components.AComponent;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXDatabaseDataSource;
import er.extensions.eof.ERXQ;

public class ListeElementsCarriere extends AComponent {
	

	private static final long serialVersionUID = 7778897L;
	
	private static final Integer NUMBER_OF_OBJECT_PER_BATCH = 10;
	
	private static final String BINDING_CURRENT_ELEMENT = "element";
	private static final String BINDING_QUALIFIER = "qualifier";
	
	private EOElements currentElement;
	
	private ERXDisplayGroup<EOElements> displayGroupElements = null;
	private ERXDatabaseDataSource contratDatasource = null;
	private EOQualifier qualifier;
	
	private String bap;
	private String corps;
	private String echelon;
	private String grade;
	private NSTimestamp dDebAff;
	private NSTimestamp dFinAff;
	
    public ListeElementsCarriere(WOContext context) {
        super(context);
    }
    
    
    /**
	 * @return the displayGroupContrat
	 */
	public ERXDisplayGroup<EOElements> getDisplayGroupElements() {
		if (displayGroupElements == null) {
			displayGroupElements = new ERXDisplayGroup<EOElements>();
			displayGroupElements.setDataSource(contratDatasource());
			displayGroupElements.setDelegate(new DisplayGroupContratDelegate());
			
			
			NSArray<EOSortOrdering> sortOrderings = new NSMutableArray<EOSortOrdering>();
			sortOrderings.add(EOSortOrdering.sortOrderingWithKey(EOElements.D_EFFET_ELEMENT_KEY, EOSortOrdering.CompareCaseInsensitiveDescending));
			displayGroupElements.setSortOrderings(sortOrderings);

			displayGroupElements.setSelectsFirstObjectAfterFetch(true);
			displayGroupElements.setNumberOfObjectsPerBatch(NUMBER_OF_OBJECT_PER_BATCH);
			displayGroupElements.fetch();
			
		}

		return displayGroupElements;
	}
	
	/**
	 * delegate class handle the current selected item
	 */
	public class DisplayGroupContratDelegate {

		/**
		 * @param group : groupe d'élement sélectionné
		 */
		public void displayGroupDidChangeSelectedObjects(final WODisplayGroup group) {
			@SuppressWarnings("unchecked")
			ERXDisplayGroup<EOElements> groupe = (ERXDisplayGroup<EOElements>) group;
		}
	}

	/**
	 * @return datasource des affectations
	 */
	private ERXDatabaseDataSource contratDatasource() {
		if (contratDatasource == null) {
			contratDatasource = new ERXDatabaseDataSource(edc(), EOElements.ENTITY_NAME);
			contratDatasource.setAuxiliaryQualifier(getQualifier());
		}
		return contratDatasource;
	}
    
	/**
	 * @return the qualifier
	 */
	public EOQualifier getQualifier() {
		qualifier = (EOQualifier) valueForBinding(BINDING_QUALIFIER);
		qualifier = ERXQ.and(new NSArray<EOQualifier>(new EOQualifier[] {
				qualifier,
				ERXQ.equals(EOElements.TEM_VALIDE_KEY, "O")
		}));
		return qualifier;
	}
    
    
    /**
	 * @return the currentElement
	 */
    public EOElements getCurrentElement() {
    	if (hasBinding(BINDING_CURRENT_ELEMENT)) {
			currentElement = (EOElements) valueForBinding(BINDING_CURRENT_ELEMENT);
		}
		return currentElement;
	}

    /**
	 * @param currentElement the currentElement to set
	 */
	public void setCurrentElement(EOElements currentElement) {
		setValueForBinding(currentElement, BINDING_CURRENT_ELEMENT);
		this.currentElement = currentElement;
	}

	/**
	 * 
	 * @return l'ID du composant
	 */
	public String getListeElementsTableViewId() {
		return getComponentId() + "_listeContratsTableViewId";
	}
	
	/* **************************************************************************************************** */
	/*			Méthodes d'accès aux informations de l'élément de carrière, résultats de la recherche		*/
	/* **************************************************************************************************** */
	
	public String getGrade() {
		if (getCurrentElement() != null) {
			grade = getCurrentElement().toGrade().llGrade();
		}
		return grade;
	}
	
	public String getCorps() {
		if (getCurrentElement() != null) {
			corps = getCurrentElement().toCorps().llCorps();
		}
		return corps;
	}
	
	public String getEchelon() {
		if (getCurrentElement() != null) {
			echelon = getCurrentElement().cEchelon();
		}
		return echelon;
	}
	
	public String getBap() {
		if (getCurrentElement() != null) {
			bap = getCurrentElement().cBap();
		}
		return bap;
	}
	
	
	/**
	 * @return la date de début de l'affectation
	 */
	public NSTimestamp getDateEffetElement() {
		if (getCurrentElement() != null) {
			dDebAff = getCurrentElement().dEffetElement();
		}
		return dDebAff;
	}
	
	/**
	 * @return la date de fin de l'affectation
	 */
	public NSTimestamp getDateFinElement() {
		if (getCurrentElement() != null) {
			dFinAff = getCurrentElement().dFinElement();
		}
		return dFinAff;
	}

	/**
	 * Test de la nullité ou non de la date de fin
	 * @return true si la date de fin est nulle
	 */
	public boolean isDateFinNulle() {
		if (getCurrentElement().dFinElement() == null) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * Retourne la String de format des dates
	 * @return le format par défaut pour les dates
	 */
	public String getTimestampFormatter() {
		return DateCtrl.DEFAULT_FORMAT;
	}
	
    /* **************************************************************************************************** */
	/*										Partie des WOActionResult										*/
	/* **************************************************************************************************** */
    
    /**
	 * mise à jour de l'interface
	 * @return null (reste sur la page)
	 */
	public WOActionResults update() {
		return doNothing();
	}
    
    
    
    
}