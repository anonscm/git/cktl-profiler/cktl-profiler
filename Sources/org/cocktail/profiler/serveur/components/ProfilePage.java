/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.profiler.serveur.components;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.profiler.serveur.Application;
import org.cocktail.profiler.serveur.ProfilerParamManager;
import org.cocktail.profiler.serveur.ProfilerUser;
import org.cocktail.profiler.serveur.Session;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.foundation.NSArray;

import er.ajax.AjaxModalDialog;

public class ProfilePage extends ProfilBaseCmp {

	/** Serial version UID. */
	private static final long serialVersionUID = 1L;

	private String searchTypeIntExt = "interne";
	private String searchTypePhysMoral = "individu";
	private boolean isInSearch;
	private NSArray<String> lstRefreshSearchZones = new NSArray<String>(new String[] {"aucSearch"});
	private IPersonne selectedSearchPersonne;


	/**
	 * Constructeur.
	 * @param context context WO.
	 */
	public ProfilePage(WOContext context) {
		super(context);
	}

	@Override
	public void appendToResponse(WOResponse woresponse, WOContext wocontext) {
		super.appendToResponse(woresponse, wocontext);
	}

	public WOActionResults doNothing() {
		return null;
	}

	public String classInfosPerso() {
		if (InfosPerso.class.getName().equals(
				((Session) session()).getSelectedOnglet())) {
			return "selectedTab";
		}
		return "";
	}

	public WOActionResults goInfosPro() {
		((Session) session()).setSelectedOnglet(InfosPro.class.getName());
		return null;
	}
	
	public WOActionResults goInfosRetraite() {
		((Session) session()).setSelectedOnglet(InfosRetraite.class.getName());
		return null;
	}

	public String classEtatCivil() {
		if (EtatCivil.class.getName().equals(
				((Session) session()).getSelectedOnglet())) {
			return "selectedTab";
		}
		return "";
	}

	public WOActionResults goAnnuaire() {
		((Session) session()).setSelectedOnglet(Annuaire.class.getName());
		return null;
	}

	public WOActionResults goEtatCivil() {
		((Session) session()).setSelectedOnglet(EtatCivil.class.getName());
		return null;
	}

	public WOActionResults goInfosScol() {
		((Session) session()).setSelectedOnglet(Scolarite.class.getName());
		return null;
	}

	public String classInfosPro() {
		if (InfosPro.class.getName().equals(
				((Session) session()).getSelectedOnglet())) {
			return "selectedTab";
		}
		return "";
	}
	
	public String classInfosRetraite() {
		if (InfosRetraite.class.getName().equals(
				((Session) session()).getSelectedOnglet())) {
			return "selectedTab";
		}
		return "";
	}

	public String classInfosScol() {
		if (Scolarite.class.getName().equals(
				((Session) session()).getSelectedOnglet())) {
			return "selectedTab";
		}
		return "";
	}

	public String classAnnuaire() {
		if (Annuaire.class.getName().equals(
				((Session) session()).getSelectedOnglet())) {
			return "selectedTab";
		}
		return "";
	}
	public String classAutres() {
		if (Autres.class.getName().equals(
				((Session) session()).getSelectedOnglet())) {
			return "selectedTab";
		}
		return "";
	}
	public WOActionResults goAutres() {
		((Session) session()).setSelectedOnglet(Autres.class.getName());
		return null;
	}

	public WOActionResults goInfosPerso() {
		((Session) session()).setSelectedOnglet(InfosPerso.class.getName());
		return null;
	}

	public String classCompte() {
		if (InfosCompte.class.getName().equals(
				((Session) session()).getSelectedOnglet())) {
			return "selectedTab";
		}
		return "";
	}

	public WOActionResults goCompte() {
		((Session) session()).setSelectedOnglet(InfosCompte.class.getName());
		return null;
	}

	public WOActionResults selectFromSearch() {
		((Session) session()).setSelectedPersonne(selectedSearchPersonne());
		((Session) session()).resetDatas();
		return pageWithName(ProfilePage.class.getName());
	}

	public ProfilerUser getPUser() {
		return ((Session) session()).getPUser();
	}

	/**
	 * @return the searchTypeIntExt
	 */
	public String searchTypeIntExt() {
		return searchTypeIntExt;
	}

	/**
	 * @param searchTypeIntExt
	 *            the searchTypeIntExt to set
	 */
	public void setSearchTypeIntExt(String searchTypeIntExt) {
		this.searchTypeIntExt = searchTypeIntExt;
	}

	/**
	 * @return the searchTypePhysMoral
	 */
	public String searchTypePhysMoral() {
		return searchTypePhysMoral;
	}

	/**
	 * @param searchTypePhysMoral
	 *            the searchTypePhysMoral to set
	 */
	public void setSearchTypePhysMoral(String searchTypePhysMoral) {
		this.searchTypePhysMoral = searchTypePhysMoral;
	}

	public WOActionResults openSearch() {
		getEc().invalidateAllObjects();
		AjaxModalDialog.open(context(), "amdSearch", "Recherche ...");
		setInSearch(true);
		return null;
	}

	public WOActionResults onCloseSearch() {
		setInSearch(false);

		return null;
	}

	/**
	 * @return the isInSearch
	 */
	public boolean isInSearch() {
		return isInSearch;
	}

	/**
	 * @param isInSearch
	 *            the isInSearch to set
	 */
	public void setInSearch(boolean isInSearch) {
		this.isInSearch = isInSearch;
	}

	public String aucTriggerSearchId() {

		return getComponentId() + "_aucTriggerSearchId";
	}

	/**
	 * @return the lstRefreshSearchZones
	 */
	public NSArray<String> lstRefreshSearchZones() {
		return lstRefreshSearchZones;
	}

	/**
	 * @param lstRefreshSearchZones
	 *            the lstRefreshSearchZones to set
	 */
	public void setLstRefreshSearchZones(NSArray<String> lstRefreshSearchZones) {
		this.lstRefreshSearchZones = lstRefreshSearchZones;
	}

	public String aucselpersonneid() {
		return getComponentId() + "_aucselpersonneid";
	}

	/**
	 * @return the selectedSearchPersonne
	 */
	public IPersonne selectedSearchPersonne() {
		return selectedSearchPersonne;
	}

	/**
	 * @param selectedSearchPersonne
	 *            the selectedSearchPersonne to set
	 */
	public void setSelectedSearchPersonne(IPersonne selectedSearchPersonne) {
		this.selectedSearchPersonne = selectedSearchPersonne;
	}

	public boolean canShowScol() {
		return ((Application) application()).config().booleanForKey(Application.SHOW_SCOL)
				&& (((((EOIndividu) ((Session) session()).selectedPersonne()).isEtudiant())
						&& (((Session) session()).lstInscDipl() != null)
						&& (((Session) session()).lstInscDipl().size() > 0))
				|| ((EOIndividu) ((Session) session()).selectedPersonne()).isDoctorant()
						&& (((Session) session()).lstInscDipl() != null)
						&& (((Session) session()).lstInscDipl().size() > 0));

	}

	public boolean canSeePersIdNoInd() {
		if (((Session) session()).selectedPersonne().getNumero().equals(getPUser().getNoIndividu().toString())
				|| getPUser().haveAdminRight()) {
			return true;
		}
		return false;
	}
	
	/**
	 * Paramétrage de l'accès à l'onglet "Annuaire" pour savoir si on veut l'utiliser et ensuite si les étudiants y ont accès
	 * @return true si on peut accéder à l'annuaire
	 */
	public boolean canSeeAnnuaire() {
		if (!((Application) application()).config().booleanForKey(ProfilerParamManager.PROFILER_ONGLET_ANNUAIRE_ACTIVE)) {
			return false;
		}

		if (((EOIndividu) ((Session) session()).selectedPersonne()).isEtudiant()) {
			if (((Application) application()).config().booleanForKey(ProfilerParamManager.PROFILER_ANNUAIRE_ACCES_ETUDIANT_ACTIVE)) {
				return true;
			} else {
				return false;
			}
		}

		return true;
	}
	
	/**
	 * Paramétrage du blocage de l'onglet "Autres"
	 * @return true si on peut accéder à l'annuaire
	 */
	public boolean canSeeAutres() {
		if (((Application) application()).config().booleanForKey(ProfilerParamManager.PROFILER_ONGLET_AUTRES_ACTIVE)) {
			return true;
		}
		return false;
	}
	
	/**
	 * Paramétrage du blocage de l'onglet "Infos retraite"
	 * @return true si on peut accéder aux informations du CIR
	 */
	public boolean canSeeInfosCir() {
		String valueParam = ((Application) application()).config().stringForKey(ProfilerParamManager.PROFILER_ONGLET_CIR_ACTIVE);
		if (valueParam == null) {
			return true;
		}
		if (StringCtrl.toBool(valueParam)) {
			return true;
		}
		return false;
	}
	
	/**
	 * Paramétrage du blocage de l'onglet "Comptes"
	 * @return true si on souhaite voir l'onglet "Comptes"
	 */
	public boolean canSeeComptes() {
		if (((Application) application()).config().booleanForKey(ProfilerParamManager.PROFILER_ONGLET_COMPTES_ACTIVE)) {
			return true;
		}
		return false;
	}
	
	public boolean isOngletScolariteAccessible() {
		if (myApp().config().booleanForKey(ProfilerParamManager.PROFILER_ONGLET_SCOL_ACTIVE)) {
			return true;
		}
		return false;
	}
	
	public boolean isOngletInfosProAccessible() {
		if (myApp().config().booleanForKey(ProfilerParamManager.PROFILER_ONGLET_INFO_PRO_ACTIVE)) {
			return true;
		}
		return false;
	}
	
	public boolean isOngletInfosAdministrativesAccessibles() {
		if (myApp().config().booleanForKey(ProfilerParamManager.PROFILER_ONGLET_INFO_ADMINISTRATIVE_ACTIVE)) {
			return true;
		}
		return false;
	}
	
}