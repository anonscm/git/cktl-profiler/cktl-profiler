package org.cocktail.profiler.serveur.components;

import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlgrh.common.metier.EOAffectation;
import org.cocktail.fwkcktlgrh.common.metier.EOCarriere;
import org.cocktail.fwkcktlgrh.common.metier.EOElements;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOCompte;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlpersonne.server.util.CompteService;
import org.cocktail.fwkcktlpersonneguiajax.serveur.FwkCktlPersonneGuiAjaxParamManager;
import org.cocktail.fwkgspot.serveur.metier.eof.EOTypeOccupation;
import org.cocktail.profiler.serveur.Application;
import org.cocktail.profiler.serveur.ProfilerParamManager;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

public class EditResearchedPerson extends ProfilBaseCmp {

	private static final long serialVersionUID = 112233445566L;
	public static final String BDG_RESEARCHED_PERSONNE = "researchedPerson";
	public static final String BDG_EDITING_CONTEXT = "editingContext";
	public static final String BDG_RESET = "wantReset";

	private EOPersonneTelephone unTel;
	private EOPersonneTelephone unFax;
	private Boolean hasGspotTables = null;
	private boolean wantRefresh;

	private String imWidth;
	private String imHeight;

	NSDictionary<String, String> dictionnaire = null;

	public EditResearchedPerson(WOContext context) {
        super(context);
        this.wantRefresh = true;
    }

	@Override
	public void appendToResponse(WOResponse woresponse, WOContext wocontext) {
		this.wantRefresh = true;
		dictionnaire = initDico();
		super.appendToResponse(woresponse, wocontext);
	}

	public EOEditingContext editingContext(){
		if (hasBinding(BDG_EDITING_CONTEXT)) {
			return (EOEditingContext) valueForBinding(BDG_EDITING_CONTEXT);
		} else {
			return getResearchedPersonne().editingContext();
		}

	}

	public boolean getWantReset(){
		if (hasBinding(BDG_RESET)) {
			return (Boolean) valueForBinding(BDG_RESET);
		} else {
			return wantRefresh;
		}
	}

	public void setWantReset(boolean wantResetValue){
		if (hasBinding(BDG_RESET)) {
			setValueForBinding(false, BDG_RESET);
		} else {
			this.wantRefresh = wantResetValue;
		}
	}

	public IPersonne getResearchedPersonne() {
		return (IPersonne) valueForBinding(BDG_RESEARCHED_PERSONNE);
	}

	public void setResearchedPersonne(IPersonne researchedPersonne) {
		setValueForBinding(researchedPersonne, BDG_RESEARCHED_PERSONNE);
	}

	public EOIndividu getResearchedIndividu() {
		return (EOIndividu) getResearchedPersonne();
	}

	public EOPersonneTelephone getUnTel() {
		return unTel;
	}

	public void setUnTel(EOPersonneTelephone unTel) {
		this.unTel = unTel;
	}

	public EOPersonneTelephone getUnFax() {
		return unFax;
	}

	public void setUnFax(EOPersonneTelephone unFax) {
		this.unFax = unFax;
	}


	public String getImHeight() {
		if (imHeight == null) {
			 imHeight = EOGrhumParametres.parametrePourCle(editingContext(), FwkCktlPersonneGuiAjaxParamManager.GRHUM_PHOTO_HEIGHT);
		}
		return imHeight;
	}

	public void setImHeight(String imHeight) {
		this.imHeight = imHeight;
	}

	public String getImWidth() {
		if (imWidth == null) {
			 imWidth = EOGrhumParametres.parametrePourCle(editingContext(), FwkCktlPersonneGuiAjaxParamManager.GRHUM_PHOTO_HEIGHT);
			 imWidth = myApp().config().stringForKey("IMAGE_WIDTH");
		}
		return imWidth;
	}

	public void setImWidth(String imWidth) {
		this.imWidth = imWidth;
	}


	public EORepartPersonneAdresse selectedRepartPersonneAdresse(){
		NSArray<EORepartPersonneAdresse> repartPersonneAdresse = (NSArray<EORepartPersonneAdresse>) EORepartPersonneAdresse.adressesValidesDeType(editingContext(), getResearchedIndividu(), "PRO");
		if (repartPersonneAdresse.count() > 0) {
				return repartPersonneAdresse.objectAtIndex(0);
			} else {
				return null;
			}
	}

	// Recherche du compte prioritaire
	public EOCompte getComptePrioritaire(EOIndividu individu) {
		NSArray<EOCompte> arrayComptes = individu.toComptes(EOCompte.QUAL_CPT_VALIDE_OUI, true);
		EOCompte compte = null;
		CompteService compteService = new CompteService();
		if (!compteService.classerParPriorite(arrayComptes).isEmpty()) {
			compte = (EOCompte) compteService.classerParPriorite(arrayComptes).objectAtIndex(0);
		}
//		EOCompte compte = (EOCompte) EOCompte.sortedPriority(arrayComptes).objectAtIndex(0);
		if (compte != null) {
			return compte;
		} else {
			return null;
		}
	}

	// Renvoi l'email professionnel (le premier trouvé d'après les priorités)
	public String getEmail() {
		EOIndividu individu = getResearchedIndividu();
		if (getComptePrioritaire(individu) != null && getComptePrioritaire(individu).toCompteEmail() != null) {
			String email = getComptePrioritaire(individu).toCompteEmail().cemEmail();
			email += "@" + getComptePrioritaire(individu).toCompteEmail().cemDomaine();
			return email;
		} else {
			return null;
		}
	}

	public boolean hasAdressPro(){
		if (selectedRepartPersonneAdresse() == null) {
			return false;
		}
		return true;
	}

	public boolean hasEmailPro(){
		if (getEmail() == null) {
			return false;
		}
		return true;
	}

	// Renvoi le numéro de téléphone professionnel ou le personnel en fonction du type
	public NSArray<EOPersonneTelephone> getListeTelPro(){
		EOIndividu individu = getResearchedIndividu();
		EOQualifier myQualifier = null;

		NSArray<EOPersonneTelephone> telephones;

		if (((Application) application()).config().booleanForKey(ProfilerParamManager.PROFILER_TEL_LISTE_ROUGE_BLOCAGE_ACTIVE)) {
			myQualifier = EOPersonneTelephone.PERS_ID.eq(individu.persId()).and(EOPersonneTelephone.TYPE_TEL.eq("PRF").and(EOPersonneTelephone.TYPE_NO.eq("TEL").and(EOPersonneTelephone.QUAL_PERSONNE_SANS_NUM_LISTE_ROUGE)));
		} else {
			myQualifier = EOPersonneTelephone.PERS_ID.eq(individu.persId()).and(EOPersonneTelephone.TYPE_TEL.eq("PRF").and(EOPersonneTelephone.TYPE_NO.eq("TEL")));
		}

		telephones = EOPersonneTelephone.fetchAll(editingContext(), myQualifier);

		if (telephones.count() > 0) {
			return telephones;
		} else {
			return null;
		}
	}

	public boolean hasTelPro(){
		if (getListeTelPro() == null || getListeTelPro().count() < 1) {
			return false;
		}
		return true;
	}

	// Renvoi le numéro de fax professionnel
	public NSArray<EOPersonneTelephone> getListeFaxPro(){
		EOIndividu individu = getResearchedIndividu();
		EOQualifier myQualifier;
		NSArray<EOPersonneTelephone> telephones;

		if (((Application) application()).config().booleanForKey(ProfilerParamManager.PROFILER_TEL_LISTE_ROUGE_BLOCAGE_ACTIVE)) {
			myQualifier = EOPersonneTelephone.PERS_ID.eq(individu.persId()).and(EOPersonneTelephone.TYPE_TEL.eq("PRF").and(EOPersonneTelephone.TYPE_NO.eq("FAX").and(EOPersonneTelephone.QUAL_PERSONNE_SANS_NUM_LISTE_ROUGE)));
		} else {
			myQualifier = EOPersonneTelephone.PERS_ID.eq(individu.persId()).and(EOPersonneTelephone.TYPE_TEL.eq("PRF").and(EOPersonneTelephone.TYPE_NO.eq("FAX")));
		}

		telephones = EOPersonneTelephone.fetchAll(editingContext(), myQualifier);

		if (telephones.count() > 0) {
			return telephones;
		} else {
			return null;
		}
	}

	public boolean hasFaxPro(){
		if (getListeFaxPro() == null || getListeFaxPro().count() < 1) {
			return false;
		}
		return true;
	}

	 /**
     * @return true si les tables de Gspot sont présentes.
     *          Il faut voir si à la place, on ne peut pas carrément forcer
     *          une vérif de dépendance au démarrage via les versions de bdd...
     */
    public Boolean hasGspotTables() {
        if (hasGspotTables == null) {
            try {
                EOTypeOccupation.fetchAllFwkGspot_TypeOccupations(getEc());
                hasGspotTables = Boolean.TRUE;
            } catch (Exception e) {
                hasGspotTables = Boolean.FALSE;
                logger().error(e.getMessage(), e);
            }
        }
        return hasGspotTables;
    }


    public boolean hasPhoto(){
    	if (getResearchedIndividu().toPhoto() == null || getResearchedIndividu().toPhoto().datasPhoto() == null) {
    		return false;
    	}
    	
    	// On teste si la photo n'est pas privée
    	if (MyStringCtrl.isChaineOui(getResearchedIndividu().indPhoto())) {
    		return true;
    	}
        return false;

    }



    /* ********************************************************************************************************************** */
	// Renvoi un dictionnaire avec l'affectation renseignée pour la carrière
	@SuppressWarnings("unchecked")
	public NSDictionary<String, String> getAffectation(EOIndividu individu, NSTimestamp dateAffect) {
		EOQualifier myQualifier;
		EOFetchSpecification myFetch;
		NSMutableDictionary<String, String> myMDico = new NSMutableDictionary<String, String>();
		NSArray<EOAffectation> localResult;

		@SuppressWarnings("rawtypes")
		NSMutableArray args = new NSMutableArray(individu.noIndividu());
		args.addObject(dateAffect);
		args.addObject(dateAffect);

		myQualifier = EOQualifier.qualifierWithQualifierFormat("noDossierPers=%@ and dDebAffectation<=%@ and (dFinAffectation>=%@ or dFinAffectation=nil)", args);
		myFetch = new EOFetchSpecification(EOAffectation.ENTITY_NAME, myQualifier, null);

		localResult = editingContext().objectsWithFetchSpecification(myFetch);
		if (localResult.count() > 0) {
			EOAffectation affectation = localResult.objectAtIndex(0);
			myMDico.setObjectForKey(affectation.toStructure().libelle(), "Service");
			EOStructure composante = EOStructure.getComposante(editingContext(), affectation.toStructure().cStructure());
			if (composante != null) {
				myMDico.setObjectForKey(composante.libelle(), "Composante");
			}
			return myMDico.immutableClone();
		}

		return null;
	}

	// Renvoi un élément de carrière déterminé par une date
	public NSDictionary<String, String> getCarriere(EOIndividu individu, NSTimestamp dateCarr){
		NSMutableDictionary<String, String> myMDico = new NSMutableDictionary<String, String>();
		NSArray<EOCarriere> localResult;
		localResult = EOCarriere.carrieresForIndividu(editingContext(), individu);

		if (localResult.count() > 0) {
//			EOCarriere eltCarriere = localResult.objectAtIndex(0);
//			myMDico.setObjectForKey(eltCarriere.toElements().objectAtIndex(0).cCorps(), "codeCorps");
//			myMDico.setObjectForKey(eltCarriere.toElements().objectAtIndex(0).toCorps().lcCorps(), "lcCorps");
//			myMDico.setObjectForKey(eltCarriere.toElements().objectAtIndex(0).toCorps().llCorps(), "llCorps");
			EOElements eltCarriere = EOElements.elementCourant(editingContext(), individu);
			if (eltCarriere != null) {
				myMDico.setObjectForKey(eltCarriere.toCorps().cCorps(), "codeCorps");
				myMDico.setObjectForKey(eltCarriere.toCorps().lcCorps(), "lcCorps");
				myMDico.setObjectForKey(eltCarriere.toCorps().llCorps(), "llCorps");

				return myMDico.immutableClone();
			}
			
		}

		return null;
	}

	// Renvoi les infos sur le contact (son rôle et son entreprise d'appartenance)
	public NSDictionary<String, String> getInfosContact(EOIndividu individu){
		EOQualifier myQualifier;
		NSMutableDictionary<String, String> myMDico = new NSMutableDictionary<String, String>();
		NSArray<EORepartAssociation> localResult;

		myQualifier = EORepartAssociation.PERS_ID.eq(individu.persId()).and(EORepartAssociation.TO_ASSOCIATION.dot(EOAssociation.ASS_CODE).eq("CONTA"));

		localResult = EORepartAssociation.fetchAll(editingContext(), myQualifier);

		if (localResult.count() > 0) {
			EOGenericRecord recordRepartAsso = (EOGenericRecord) localResult.objectAtIndex(0);
			String fonction = ((EORepartAssociation) recordRepartAsso).toAssociation().assLibelle();

			String entreprise = "" +  ((EORepartAssociation) recordRepartAsso).toStructure().libelle();

			myMDico.setObjectForKey(fonction, "Fonction Individu");
			myMDico.setObjectForKey(entreprise, "Entreprise");

			return myMDico.immutableClone();
		}

		return null;
	}

	// Création d'un dictionnaire
	public NSDictionary<String, String> initDico(){
		EOIndividu individu = getResearchedIndividu();
		NSMutableDictionary<String, String> myDico = new NSMutableDictionary<String, String>();
		NSDictionary<String, String> myDicoTemp = new NSDictionary<String, String>();

		if (individu != null) {
			if (getAffectation(individu, new NSTimestamp()) != null) {
				myDicoTemp = getAffectation(individu, new NSTimestamp());
				myDico.addEntriesFromDictionary(myDicoTemp);
				mySession().addSimpleInfoMessage("Infos Individus", "Affectation trouvée");
			}

			if (getCarriere(individu, new NSTimestamp()) != null) {
				myDicoTemp = getCarriere(individu, new NSTimestamp());
				myDico.addEntriesFromDictionary(myDicoTemp);
				mySession().addSimpleInfoMessage("Infos Individus", "Carrière trouvée");
			}
			if (getInfosContact(individu) != null) {
				myDicoTemp = getInfosContact(individu);
				myDico.addEntriesFromDictionary(myDicoTemp);
				mySession().addSimpleInfoMessage("Infos Individus", "Infos contact trouvées");
			}
		} else {
			mySession().addSimpleErrorMessage("Info Individu", "L'individu de la recherche est null!");
		}

		return myDico.immutableClone();
	}



//	NSDictionary<String, String> dictionnaire = initDico();

	/**
	 * Retourne true si le Service auquel l'individu appartient est renseigné
	 */
	public boolean hasService(){
		return hasEntry(dictionnaire, "Service");
	}

	/**
	 * Retourne le Service auquel l'individu appartient
	 */
	public String getService(){
		String donnee = noString();
		if (hasEntry(dictionnaire, "Service")) {
			donnee = dictionnaire.get("Service");
		}
		return donnee;
	}

	/**
	 * Retourne true si la Composante à laquelle l'individu est rattaché est renseigné
	 */
	public boolean hasComposante(){
		return hasEntry(dictionnaire, "Composante");
	}

	/**
	 * Retourne la composante à laquelle l'individu est rattaché
	 */
	public String getComposante(){
		String donnee = noString();
		if (hasEntry(dictionnaire, "Composante")) {
			donnee = dictionnaire.get("Composante");
		}
		return donnee;
	}

	/**
	 * Retourne true si le Corps d'affectation de l'individu est renseigné
	 */
	public boolean hasCorps(){
		return hasEntry(dictionnaire, "lcCorps");
	}

	/**
	 * Retourne le corps auquel l'individu appartient
	 */
	public String getCorps(){
		String donnee = noString();
		if (hasEntry(dictionnaire, "lcCorps")) {
			donnee = dictionnaire.get("lcCorps");
		}
		return donnee;
	}

	/**
	 * Retourne true si une fonction est trouvée
	 */
	public boolean hasFonction(){
		return hasEntry(dictionnaire, "Fonction Individu");
	}

	/**
	 * Retourne la fonction occupée
	 */
	public String getFonction(){
		String donnee = noString();
		if (hasEntry(dictionnaire, "Fonction Individu")) {
			donnee = dictionnaire.get("Fonction Individu");
		}
		return donnee;
	}

	/**
	 * Méthode classique pour renvoyer une chaîne sans info
	 * @return "---"
	 */

	public String noString() {
		return "---";
	}

	private boolean hasEntry(NSDictionary dictionnary, String entryName) {
		if (dictionnary == null) {
			return false;
		}
		return !dictionnary.isEmpty() && dictionnary.get(entryName) != null;
	}
}