package org.cocktail.profiler.serveur.components;

import org.cocktail.fwkcktlgrh.common.metier.EOContrat;
import org.cocktail.fwkcktlpersonneguiajax.serveur.components.AComponent;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXDatabaseDataSource;
import er.extensions.eof.ERXQ;

public class ListeContrats extends AComponent {
	
	private static final long serialVersionUID = 7778897L;
	
	private static final Integer NUMBER_OF_OBJECT_PER_BATCH = 10;
	
	private static final String BINDING_CURRENT_CONTRAT = "contrat";
	private static final String BINDING_QUALIFIER = "qualifier";
	
	private EOContrat currentContrat;
	
	private String typeContrat;
	private NSTimestamp dDebAff;
	private NSTimestamp dFinAff;
	
	private ERXDisplayGroup<EOContrat> displayGroupContrats = null;
	private ERXDatabaseDataSource contratDatasource = null;
	private EOQualifier qualifier;
	
    public ListeContrats(WOContext context) {
        super(context);
    }
    
    /**
	 * @return the displayGroupContrat
	 */
	public ERXDisplayGroup<EOContrat> getDisplayGroupContrats() {
		if (displayGroupContrats == null) {
			displayGroupContrats = new ERXDisplayGroup<EOContrat>();
			displayGroupContrats.setDataSource(contratDatasource());
			displayGroupContrats.setDelegate(new DisplayGroupContratDelegate());
			
			
			NSArray<EOSortOrdering> sortOrderings = new NSMutableArray<EOSortOrdering>();
			sortOrderings.add(EOSortOrdering.sortOrderingWithKey(EOContrat.D_DEB_CONTRAT_TRAV_KEY, EOSortOrdering.CompareCaseInsensitiveDescending));
			displayGroupContrats.setSortOrderings(sortOrderings);

			displayGroupContrats.setSelectsFirstObjectAfterFetch(true);
			displayGroupContrats.setNumberOfObjectsPerBatch(NUMBER_OF_OBJECT_PER_BATCH);
			displayGroupContrats.fetch();
			
		}

		return displayGroupContrats;
	}
	
	/**
	 * delegate class handle the current selected item
	 */
	public class DisplayGroupContratDelegate {

		/**
		 * @param group : groupe d'élement sélectionné
		 */
		public void displayGroupDidChangeSelectedObjects(final WODisplayGroup group) {
			@SuppressWarnings("unchecked")
			ERXDisplayGroup<EOContrat> groupe = (ERXDisplayGroup<EOContrat>) group;
		}
	}

	/**
	 * @return datasource des contrats
	 */
	private ERXDatabaseDataSource contratDatasource() {
		if (contratDatasource == null) {
			contratDatasource = new ERXDatabaseDataSource(edc(), EOContrat.ENTITY_NAME);
			contratDatasource.setAuxiliaryQualifier(getQualifier());
		}
		return contratDatasource;
	}
    
	/**
	 * @return the qualifier sur les contrats
	 */
	public EOQualifier getQualifier() {
		qualifier = (EOQualifier) valueForBinding(BINDING_QUALIFIER);
		qualifier = ERXQ.and(new NSArray<EOQualifier>(new EOQualifier[] {
				qualifier,
				ERXQ.equals(EOContrat.TEM_ANNULATION_KEY, "N")
		}));
		return qualifier;
	}
    
	
	/**
	 * @return the currentContrat
	 */
	public EOContrat getCurrentContrat() {
		if (hasBinding(BINDING_CURRENT_CONTRAT)) {
			currentContrat = (EOContrat) valueForBinding(BINDING_CURRENT_CONTRAT);
		}
		return currentContrat;
	}

	/**
	 * @param currentContrat the currentContrat to set
	 */
	public void setCurrentContrat(EOContrat currentContrat) {
		setValueForBinding(currentContrat, BINDING_CURRENT_CONTRAT);
		this.currentContrat = currentContrat;
	}

	/**
	 * 
	 * @return l'ID du composant
	 */
	public String getListeContratsTableViewId() {
		return getComponentId() + "_listeContratsTableViewId";
	}
	
	/* **************************************************************************************************** */
	/*					Méthodes d'accès aux informations des contrats, résultats de la recherche		*/
	/* **************************************************************************************************** */
	
	/**
	 * @return une Strin avec le type de contrat
	 */
	public String getTypeContrat() {
		if (getCurrentContrat() != null) {
			typeContrat = getCurrentContrat().toTypeContratTravail().llTypeContratTrav();
		}
		return typeContrat;
	}
	
	
	/**
	 * @return la date de début de l'affectation
	 */
	public NSTimestamp getDateDebContrat() {
		if (getCurrentContrat() != null) {
			dDebAff = getCurrentContrat().dDebContratTrav();
		}
		return dDebAff;
	}
	
	/**
	 * @return la date de fin de l'affectation
	 */
	public NSTimestamp getDateFinContrat() {
		if (getCurrentContrat() != null) {
			dFinAff = getCurrentContrat().dFinContratTrav();
		}
		return dFinAff;
	}

	/**
	 * Test de la nullité ou non de la date de fin
	 * @return true si la date de fin est nulle
	 */
	public boolean isDateFinNulle() {
		if (getCurrentContrat().dFinContratTrav() == null) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * Retourne la String de format des dates
	 * @return le format par défaut pour les dates
	 */
	public String getTimestampFormatter() {
		return DateCtrl.DEFAULT_FORMAT;
	}
	
    /* **************************************************************************************************** */
	/*										Partie des WOActionResult										*/
	/* **************************************************************************************************** */
    
    /**
	 * mise à jour de l'interface
	 * @return null (reste sur la page)
	 */
	public WOActionResults update() {
		return doNothing();
	}
    
}