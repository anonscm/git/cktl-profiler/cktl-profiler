package org.cocktail.profiler.serveur.components;

import org.cocktail.fwkcktlgrh.common.metier.EOAbsence;
import org.cocktail.fwkcktlgrh.common.metier.EOBeneficeEtudes;
import org.cocktail.fwkcktlgrh.common.metier.EOBonifications;
import org.cocktail.fwkcktlgrh.common.metier.EOCarriere;
import org.cocktail.fwkcktlgrh.common.metier.EOChangementPosition;
import org.cocktail.fwkcktlgrh.common.metier.EOElements;
import org.cocktail.fwkcktlgrh.common.metier.EOEtudesRachetees;
import org.cocktail.fwkcktlgrh.common.metier.EOModalitesService;
import org.cocktail.fwkcktlgrh.common.metier.EOPasse;
import org.cocktail.fwkcktlgrh.common.metier.EOPeriodesMilitaires;
import org.cocktail.fwkcktlgrh.common.metier.interfaces.IChangementPosition;
import org.cocktail.fwkcktlgrh.common.metier.interfaces.IPasse;
import org.cocktail.fwkcktlgrh.common.metier.interfaces.IPeriodesMilitaires;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;

/**
 * Composant d'affichage des informations CIR d'un agent 
 * @author alainmalaplate
 *
 */
public class InfosRetraite extends ProfilBaseCmp {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5347287074799055561L;

	private EOElements element;

	private IChangementPosition position;
	private IPasse passe;
	private IPeriodesMilitaires periodesMilit;
	
	private EOAbsence absence;
	private EOModalitesService modService;
	private EOBonifications bonification;
	private EOEtudesRachetees rachatEtude;
	private EOBeneficeEtudes benefEtude;
	
	private EOQualifier qualifier;
	
	private Integer nbreCarriere = null;
	private Integer nbreElts = null;
	private Integer nbrePosition = null;
	private Integer nbrePasse = null;
	private Integer nbrePeriodesMilit = null;
	private Integer nbreModService = null;
	private Integer nbreAbsence = null;
	private Integer nbreBonifications = null;
	private Integer nbreRachatsEtude = null;
	private Integer nbreBenefEtude = null;
	
	
	
	/**
	 * Constructeur du composant d'affichage des données CIR d'un agent
	 * @param context Contexte du composant
	 */
    public InfosRetraite(WOContext context) {
        super(context);
    }
    
    
    
    // Méthodes pour la carrière
    /**
     * @return le tire et nombre d'infos dans l'accordéon des élèments de carrière
     */
    public String getTitreCarriere() {
    	String info;
    	info = " Elément(s) de Carrière  --  " + nbreInfosEltsCarrieres().toString() + " donnée(s) correspondant à " + nbreInfosCarrieres().toString() + " carrière(s)";
    	return info;
    }
    
    /**
     * @return le nombre de carrières dans l'accordéon des élèments de carrière
     */
    public Integer nbreInfosCarrieres() {
    	if (nbreCarriere == null) {
    		Integer res = null;
    		res = EOCarriere.carrieresForIndividu(getEc(), getEditedIndividu()).count();
    		if (res != null) {
    			nbreCarriere = res;
    		} else {
    			nbreCarriere = 0;
    		}
    	}
    	return nbreCarriere;
    }
    
    /**
     * @return le nombre de carrières dans l'accordéon des élèments de carrière
     */
    public Integer nbreInfosEltsCarrieres() {
    	if (nbreElts == null) {
    		Integer res = null;
    		res = EOElements.eltsCarrieresForIndividu(getEc(), getEditedIndividu()).count();
    		if (res != null) {
    			nbreElts = res;
    		} else {
    			nbreElts = 0;
    		}
    	}
    	return nbreElts;
    }
    
    /**
     * @return true pour désactiver l'accordéon des élèments de carrière si le nbre d'infos est < 1
     */
    public boolean isCarriereDisabled() {
    	if (0 < nbreInfosCarrieres().intValue()) {
    		return false;
    	}
    	return true;
    }
    
 // Méthodes pour la position
    /**
     * @return le tire et nombre d'infos dans l'accordéon des positions
     */
    public String getTitrePositions() {
    	String info;
    	info = " Positions  --  " + nbreInfosPositions().toString() + " donnée(s)";
    	return info;
    }
    
    /**
     * @return le nombre d'infos dans l'accordéon des positions
     */
    public Integer nbreInfosPositions() {
    	if (nbrePosition == null) {
    		Integer res = null;
    		res = EOChangementPosition.positionsValidesForIndividu(getEc(), getEditedIndividu()).count();
    		if (res != null) {
    			nbrePosition = res;
    		} else {
    			nbrePosition = 0;
    		}
    	}
    	return nbrePosition;
    }
    
    /**
     * @return true pour désactiver l'accordéon des positions si le nbre d'infos est < 1
     */
    public boolean isPositionsDisabled() {
    	if (0 < nbreInfosPositions().intValue()) {
    		return false;
    	}
    	return true;
    }
    
    // Méthodes pour les Services Auxiliaires
    /**
     * @return le tire et nombre d'infos dans l'accordéon des passés
     */
    public String getServicesAux() {
    	String info;
    	info = " Services auxiliaires  --  " + nbreInfosServicesAux().toString() + " donnée(s)";
    	return info;
    }
    
    /**
     * @return le nombre d'infos dans l'accordéon des passés
     */
    public Integer nbreInfosServicesAux() {
    	if (nbrePasse == null) {
    		Integer res = null;
    		res = EOPasse.passesValidesForIndividu(getEc(), getEditedIndividu()).count();
    		if (res != null) {
    			nbrePasse = res;
    		} else {
    			nbrePasse = 0;
    		}
    	}
    	return nbrePasse;
    }
    
    /**
     * @return true pour désactiver l'accordéon des passés si le nbre d'infos est < 1
     */
    public boolean isServicesAuxDisabled() {
    	if (0 < nbreInfosServicesAux().intValue()) {
    		return false;
    	}
    	return true;
    }
    
    // Méthodes pour les Modalités de Service
    /**
     * @return le tire et nombre d'infos dans l'accordéon des modalités de service
     */
    public String getModalitesService() {
    	String info;
    	info = " Modalités de service  --  " + nbreInfosModalitesService().toString() + " donnée(s)";
    	return info;
    }
    
    /**
     * @return le nombre d'infos dans l'accordéon des modalités de service
     */
    public Integer nbreInfosModalitesService() {
    	if (nbreModService == null) {
    		Integer res = null;
    		res = EOModalitesService.modalitesServiceValidesForIndividu(getEc(), getEditedIndividu()).count();
    		if (res != null) {
    			nbreModService = res;
    		} else {
    			nbreModService = 0;
    		}
    	}
    	return nbreModService;
    }
    
    /**
     * @return true pour désactiver l'accordéon des modalités de service si le nbre d'infos est > 0
     */
    public boolean isModalitesServiceDisabled() {
    	if (0 < nbreInfosModalitesService().intValue()) {
    		return false;
    	}
    	return true;
    }
    
    // Méthodes pour les Périodes Militaires
    /**
     * @return le tire et nombre d'infos dans l'accordéon des périodes militaires
     */
    public String getPeriodesMilitaires() {
    	String info;
    	info = " Périodes militaires  --  " + nbreInfosPeriodesMilitaires().toString() + " donnée(s)";
    	return info;
    }
    
    /**
     * @return le nombre d'infos dans l'accordéon des périodes militaires
     */
    public Integer nbreInfosPeriodesMilitaires() {
    	if (nbrePeriodesMilit == null) {
    		Integer res = null;
    		res = EOPeriodesMilitaires.periodesMilitairesValidesForIndividu(getEc(), getEditedIndividu()).count();
    		if (res != null) {
    			nbrePeriodesMilit = res;
    		} else {
    			nbrePeriodesMilit = 0;
    		}
    	}
    	return nbrePeriodesMilit;
    }
    
    /**
     * @return true pour désactiver l'accordéon des périodes militaires si le nbre d'infos est < 1
     */
    public boolean isPeriodesMilitairesDisabled() {
    	if (0 < nbreInfosPeriodesMilitaires().intValue()) {
    		return false;
    	}
    	return true;
    }
    
    // Méthodes pour les Congés légaux
    /**
     * @return le tire et nombre d'infos dans l'accordéon des absences
     */
    public String getCongesLegaux() {
    	String info;
    	info = " Congés légaux  --  " + nbreInfosCongesLegaux().toString() + " donnée(s)";
    	return info;
    }
    
    /**
     * @return le nombre d'infos dans l'accordéon des absences
     */
    public Integer nbreInfosCongesLegaux() {
    	if (nbreAbsence == null) {
    		Integer res = null;
    		res = EOAbsence.absencesValidesCIRForIndividu(getEc(), getEditedIndividu()).count();
    		if (res != null) {
    			nbreAbsence = res;
    		} else {
    			nbreAbsence = 0;
    		}
    	}
    	return nbreAbsence;
    }
    
    /**
     * @return true pour désactiver l'accordéon des absences si le nbre d'infos est < 1
     */
    public boolean isCongesLegauxDisabled() {
    	if (0 < nbreInfosCongesLegaux().intValue()) {
    		return false;
    	}
    	return true;
    }
    
    // Méthodes pour les Bonifications
    /**
     * @return le tire et nombre d'infos dans l'accordéon des bonifications
     */
    public String getBonifications() {
    	String info;
    	info = " Bonifications  --  " + nbreInfosBonifications().toString() + " donnée(s)";
    	return info;
    }
    
    /**
     * @return le nombre d'infos dans l'accordéon des bonifications
     */
    public Integer nbreInfosBonifications() {
    	if (nbreBonifications == null) {
    		Integer res = null;
    		res = EOBonifications.bonificationsValidesForIndividu(getEc(), getEditedIndividu()).count();
    		if (res != null) {
    			nbreBonifications = res;
    		} else {
    			nbreBonifications = 0;
    		}
    	}
    	return nbreBonifications;
    }
    
    /**
     * @return true pour désactiver l'accordéon des bonifications si le nbre d'infos est < 1
     */
    public boolean isBonificationsDisabled() {
    	if (0 < nbreInfosBonifications().intValue()) {
    		return false;
    	}
    	return true;
    }
    
    // Méthodes pour le Rachat d'études
    /**
     * @return le tire et nombre d'infos dans l'accordéon des rachats d'étude
     */
    public String getRachatEtudes() {
    	String info;
    	info = " Rachat d'études  --  " + nbreInfosRachatEtudes().toString() + " donnée(s)";
    	return info;
    }
    
    /**
     * @return le nombre d'infos dans l'accordéon des rachats d'étude
     */
    public Integer nbreInfosRachatEtudes() {
    	return new Integer(0);
    }
    
    /**
     * @return true pour désactiver l'accordéon des rachats d'étude si le nbre d'infos est < 1
     */
    public boolean isRachatEtudesDisabled() {
    	if (0 < nbreInfosRachatEtudes().intValue()) {
    		return false;
    	}
    	return true;
    }
    
    // Méthodes pour le Bénéfice d'études
    /**
     * @return le tire et nombre d'infos dans l'accordéon des bénéfices d'étude
     */
    public String getBeneficeEtudes() {
    	String info;
    	info = " Bénéfice d'études  --  " + nbreInfosBeneficeEtudes().toString() + " donnée(s)";
    	return info;
    }
    
    /**
     * @return le nombre d'infos dans l'accordéon des bénéfices d'étude
     */
    public Integer nbreInfosBeneficeEtudes() {
    	return new Integer(0);
    }
    
    /**
     * @return true pour désactiver l'accordéon des bénéfices d'étude si le nbre d'infos est < 1
     */
    public boolean isBeneficeEtudesDisabled() {
    	if (0 < nbreInfosBeneficeEtudes().intValue()) {
    		return false;
    	}
    	return true;
    }
    
    
    
 // Données pour les éléments de carrière   
	/**
	 * @return the element
	 */
	public EOElements element() {
		return element;
	}

	/**
	 * @param element the element to set
	 */
	public void setElement(EOElements element) {
		this.element = element;
	}
    
	/**
	 * @return le qualifier pour les éléments de carrière
	 */
	public EOQualifier getQualifierElement() {
		qualifier = EOElements.TO_INDIVIDU.eq(getEditedIndividu());
		
		return qualifier;
	}

   
 // Données pour les positions

	/**
	 * @return the position
	 */
	public IChangementPosition getPosition() {
		return position;
	}

	/**
	 * @param position the position to set
	 */
	public void setPosition(IChangementPosition position) {
		this.position = position;
	}
	

	/**
	 * @return le qualifier pour les changements de position
	 */
	public EOQualifier getQualifierPosition() {
		qualifier = EOChangementPosition.TO_INDIVIDU.eq(getEditedIndividu());
		
		return qualifier;
	}
	
	
	// Données pour les passés
	
	/**
	 * @return the passe
	 */
	public IPasse getPasse() {
		return passe;
	}
	
	/**
	 * @param passe the passe to set
	 */
	public void setPasse(IPasse passe) {
		this.passe = passe;
	}
	
	
	/**
	 * @return le qualifier pour les passés
	 */
	public EOQualifier getQualifierPasse() {
		qualifier = EOPasse.TO_INDIVIDU.eq(getEditedIndividu());
		
		return qualifier;
	}
	
	
	// Données pour les périodes militaires
	/**
	 * @return the periodesMilit
	 */
	public IPeriodesMilitaires getPeriodesMilit() {
		return periodesMilit;
	}
	
	/**
	 * @param periodesMilit the periodesMilit to set
	 */
	public void setPeriodesMilit(IPeriodesMilitaires periodesMilit) {
		this.periodesMilit = periodesMilit;
	}
	
	
	/**
	 * @return le qualifier pour les périodes militaires
	 */
	public EOQualifier getQualifierPeriodesMilitaires() {
		qualifier = EOPeriodesMilitaires.TO_INDIVIDU.eq(getEditedIndividu());
		
		return qualifier;
	}
	
	
	// Données pour les modalités de service
	/**
	 * @return the periodesMilit
	 */
	public EOModalitesService getModService() {
		return modService;
	}
	
	/**
	 * @param modService the modService to set
	 */
	public void setModService(EOModalitesService modService) {
		this.modService = modService;
	}
	
	
	/**
	 * @return le qualifier pour les modalités de service
	 */
	public EOQualifier getQualifierModalitesService() {
		qualifier = EOModalitesService.INDIVIDU.eq(getEditedIndividu());
		
		return qualifier;
	}
	
	
	// Données pour les absences
	/**
	 * @return the absence
	 */
	public EOAbsence getAbsence() {
		return absence;
	}
	
	/**
	 * @param absence the absence to set
	 */
	public void setAbsence(EOAbsence absence) {
		this.absence = absence;
	}
	
	// Données pour les bonifications
	/**
	 * @return the bonification
	 */
	public EOBonifications getBonification() {
		return bonification;
	}
	
	/**
	 * @param bonification the bonification to set
	 */
	public void setBonification(EOBonifications bonification) {
		this.bonification = bonification;
	}
	
	
	/**
	 * @return le qualifier pour les absences
	 */
	public EOQualifier getQualifierBonifications() {
		qualifier = EOBonifications.TO_INDIVIDU.eq(getEditedIndividu());
		
		return qualifier;
	}
}