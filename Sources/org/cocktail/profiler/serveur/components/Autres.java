package org.cocktail.profiler.serveur.components;

import java.util.UUID;

import org.cocktail.fwkcktldroitsutils.common.util.MyDateCtrl;
import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.profiler.server.eof.EOLogListeDiffusion;
import org.cocktail.profiler.server.eof.EOParamListediffusion;
import org.cocktail.profiler.serveur.Session;
import org.cocktail.profiler.serveur.finder.FinderAutres;
import org.cocktail.profiler.serveur.finder.FinderEtatCivil;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.foundation.ERXStringUtilities;
import com.webobjects.appserver.WOActionResults;

public class Autres extends ProfilBaseCmp {
	
	private static final long serialVersionUID = 1L;
	
	private EOParamListediffusion unParam;
	private NSArray<EOParamListediffusion> listeDiffusion;
	private String aucAutresId = ERXStringUtilities.safeIdentifierName("aucAutres"
			+ UUID.randomUUID());
	
	
	
	private EOLogListeDiffusion unLog;
	
    public Autres(WOContext context) {
        super(context);
    }

	/**
	 * @return the unParam
	 */
	public EOParamListediffusion getUnParam() {
		return unParam;
	}

	/**
	 * @param unParam the unParam to set
	 */
	public void setUnParam(EOParamListediffusion unParam) {
		this.unParam = unParam;
	}

	/**
	 * @return the listeDiffusion
	 */
	public NSArray<EOParamListediffusion> getListeDiffusion() {
		Session sess = (Session) session();
		if (listeDiffusion == null){
			EOEtudiant etudiant = FinderEtatCivil.etudiantForIndividu(getEc(),(EOIndividu) getEditedIndividu());
			if (etudiant!=null){

				sess.setLstParamDiff(FinderAutres.paramForIndividu(getEc(),	etudiant));
				listeDiffusion =  sess.lstParamDiff();
			}
			listeDiffusion =  sess.lstParamDiff();
		}
		
		return listeDiffusion;
	}

	/**
	 * @param listeDiffusion the listeDiffusion to set
	 */
	public void setListeDiffusion(NSArray<EOParamListediffusion> listeDiffusion) {
		this.listeDiffusion = listeDiffusion;
	}

	/**
	 * @return the unLog
	 */
	public EOLogListeDiffusion getUnLog() {
		return unLog;
	}

	/**
	 * @param unLog the unLog to set
	 */
	public void setUnLog(EOLogListeDiffusion unLog) {
		this.unLog = unLog;
	}
    
	/**
	 * @return the aucAutresId
	 */
	public String getAucAutresId() {
		return aucAutresId;
	}

	

	public WOActionResults sauverAutres() {
	  	for(int i = 0; listeDiffusion.size()>i; i++ ) {
	  		EOParamListediffusion unParam = listeDiffusion.get(i);
	  		String  valeur = "N";
	  		if(unParam.isCaseCochee()) {
	  			valeur = "O";
	  		}
	  		EOLogListeDiffusion unLogExistant = new EOLogListeDiffusion(unParam.paramId(), getEditedIndividu().noIndividu(), valeur);
	  		EOLogListeDiffusion unLogExi =  FinderAutres.getUnLog(getEc(), getEditedIndividu().noIndividu(), unParam.paramId());
	  		
	  		if (unLogExi == null || (! unLogExi.valeur().equalsIgnoreCase(unLogExistant.valeur()))) {
		  		EOLogListeDiffusion unLog = EOLogListeDiffusion.createLogListeDiffusion(getEc(), getEditedIndividu().noIndividu(), unParam.paramId(), valeur) ;
		  		unLog.setDateCreation(MyDateCtrl.now());
		  		edc().saveChanges();
				if (edc().parentObjectStore() instanceof EOEditingContext) {
					//System.out.println("   nested  " + edc().parentObjectStore());
					((EOEditingContext) edc().parentObjectStore()).saveChanges();
				}
	  		}
	    }
		return null;
	}
	/**
	 * @return the valeur
	 */
	public boolean isValeur() {
		return getUnParam().isCaseCochee();

		
	}
	
	/**
	 * @param valeur the valeur to set
	 */
	public void setValeur(boolean valeur) {
		int i = listeDiffusion.indexOfObject(unParam);
		NSMutableArray<EOParamListediffusion> listediff = new NSMutableArray<EOParamListediffusion>();
		listediff = listeDiffusion.mutableClone();
		listediff.removeObjectAtIndex(i);
		unParam.setCaseCochee(valeur);
		listediff.add(i, unParam);
		listeDiffusion = listediff.immutableClone();
		((Session)session()).setLstParamDiff(listeDiffusion);
	}

}