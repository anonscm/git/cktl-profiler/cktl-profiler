package org.cocktail.profiler.serveur.components;

import java.util.UUID;

import org.cocktail.fwkgspot.serveur.metier.eof.EOTypeOccupation;
import org.cocktail.profiler.serveur.ProfilerParamManager;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;

import er.extensions.foundation.ERXStringUtilities;

public class BureauZone extends ProfilBaseCmp {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7928198279382826811L;
    private Boolean hasGspotTables;
    
    private boolean wantReset = true;
    private static final String WANT_RESET = "wantReset";
	private String aucBureauId = ERXStringUtilities.safeIdentifierName("aucBureau" + UUID.randomUUID());
	
    public BureauZone(WOContext context) {
        super(context);
    }
    
    
    @Override
	public void appendToResponse(WOResponse woresponse, WOContext wocontext) {
//		if (wantReset()) {
//			hasGspotTables = null;
//			setWantReset(Boolean.FALSE);
//		}
		super.appendToResponse(woresponse, wocontext);
	}
    
    /**
     * @return true si les tables de Gspot sont présentes.
     *          Il faut voir si à la place, on ne peut pas carrément forcer 
     *          une vérif de dépendance au démarrage via les versions de bdd...
     */
    public Boolean hasGspotTables() {
        if (hasGspotTables == null) {
            try {
//                EOTypeOccupation.fetchAllFwkGspot_TypeOccupations(editingContext());
                EOTypeOccupation.fetchAllFwkGspot_TypeOccupations(getEc());
                hasGspotTables = Boolean.TRUE;
            } catch (Exception e) {
                hasGspotTables = Boolean.FALSE;
                logger().error(e.getMessage(), e);
            }
        }
        return hasGspotTables;
    }
    
    public boolean wantReset() {
    	if (hasBinding(WANT_RESET)) {
    		return (Boolean) valueForBinding(WANT_RESET);
    	} else {
    		return wantReset;
    	}
//    	return false;
	}

	public void setWantReset(Boolean value) {
		if (hasBinding(WANT_RESET)) {
			setValueForBinding(value, WANT_RESET);
		} else {
			this.wantReset = value;
		}
	}
	
	public Boolean peutEditerPersonneLocalisation() {
		return getPuser().haveAdminRight() && isBureauReadOnlyEnabled();
	}
    

	/**
	 * @return the aucBureauId
	 */
	public String getAucBureauId() {
		return aucBureauId;
	}

	/**
	 * @param aucBureauId the aucBureauId to set
	 */
	public void setAucBureauId(String aucBureauId) {
		this.aucBureauId = aucBureauId;
	}

	/**
	 * AJout d'un paramètre dans la table Grhum.paramètre pour que les localisation
	 * soient seulement en mode affichage.
	 */
	public boolean isBureauReadOnlyEnabled(){
		if (myApp().config().booleanForKey(ProfilerParamManager.PROFILER_BUREAU_READONLY_ACTIVE)) {
			return true;
		}
		return false;
	}
    
}