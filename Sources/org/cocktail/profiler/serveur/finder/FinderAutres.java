package org.cocktail.profiler.serveur.finder;

import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.AFinder;
import org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.profiler.server.eof.EOLogListeDiffusion;
import org.cocktail.profiler.server.eof.EOParamListediffusion;
import org.cocktail.profiler.server.eof.EOScolInscriptionEtudiant;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;

public class FinderAutres {

	public static NSArray<EOParamListediffusion> paramForIndividu(EOEditingContext ec,
			EOEtudiant etud) {
		NSMutableArray<EOParamListediffusion> paramlistdiff = new NSMutableArray<EOParamListediffusion>();
		NSMutableArray<EOParamListediffusion> paramlistdiffAffiche = new NSMutableArray<EOParamListediffusion>();
		NSArray<EOScolInscriptionEtudiant> listInsc = FinderEtatCivil.getScolInscriptionEtudiant(ec, etud);
		for (int i = 0; listInsc.size()>i; i++){
			EOQualifier qualParam = new EOKeyValueQualifier(EOParamListediffusion.VISIBILITE_KEY, EOQualifier.QualifierOperatorLike,"*"+ listInsc.get(i).fgraCode()+"*");
			paramlistdiff.addObjectsFromArray( EOParamListediffusion.fetchParamListediffusions(ec,qualParam, null));
			for (int j =0; paramlistdiff.size() > j ; j++) {
				EOParamListediffusion unParam = paramlistdiff.get(j);
				EOLogListeDiffusion unLog = getUnLog(ec, etud.toIndividu().noIndividu(), unParam.paramId());
				if (unLog != null){
					unParam.setCaseCochee(MyStringCtrl.isChaineOui(unLog.valeur()));
					paramlistdiffAffiche.add(unParam);
				} else {
					unParam.setCaseCochee(MyStringCtrl.isChaineOui(unParam.valeurDefaut()));
					paramlistdiffAffiche.add(unParam);
				}
			}
		}
	
		AFinder.removeDuplicatesInNSArray(paramlistdiffAffiche);
		
		return paramlistdiffAffiche.immutableClone() ;

	}

	public static EOLogListeDiffusion getUnLog(EOEditingContext ec, Integer noIndividu, Long paramId){
		NSMutableArray<EOLogListeDiffusion> listeLog =  new NSMutableArray<EOLogListeDiffusion>();
		EOQualifier qualLog = new EOKeyValueQualifier(EOLogListeDiffusion.NO_INDIVIDU_KEY, EOQualifier.QualifierOperatorEqual, noIndividu);
		NSArray<EOSortOrdering> sortLog =  ERXS.descInsensitive(EOLogListeDiffusion.DATE_CREATION_KEY).array();
		EOQualifier qualLogParam = new EOKeyValueQualifier(EOLogListeDiffusion.PARAM_ID_KEY, EOQualifier.QualifierOperatorEqual, paramId);
		qualLogParam = ERXQ.and(qualLog,qualLogParam );
		//listeLog
		listeLog.addObjectsFromArray(EOLogListeDiffusion.fetchLogListeDiffusions(ec, qualLogParam, sortLog));
		if (listeLog.size() > 0 ) {
			return (EOLogListeDiffusion) listeLog.get(0);
		} else {
			return null;
		}
	}
	
}
